//
//  AddBankAccountViewController.swift
//  Influencer
//
//  Created by Manjodh Singh on 05/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

protocol AddBankAccountDelegate {
    
    func didSuccessfullyAddedBankAccount()
}

class AddBankAccountViewController: UIViewController {

    var delegate : AddBankAccountDelegate?
    
    @IBOutlet weak var txtfldAccountHolderName: UITextField!
    @IBOutlet weak var txtfldbank_name: UITextField!
    @IBOutlet weak var  txtfldaccount_number: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func crossClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveBtnPressed(_ sender: Any) {
        let accountHolderNameEntered = unwrapString(string: txtfldAccountHolderName.text)
        let bank_nameEntered = unwrapString(string: txtfldbank_name.text)
        let account_numberEntered = unwrapString(string: txtfldaccount_number.text)
        
        if accountHolderNameEntered == "" || bank_nameEntered == "" || account_numberEntered == "" {
            if accountHolderNameEntered == "" {
                SVProgressHUD.showError(withStatus: "Please enter account holder name to continue")
            }else  if bank_nameEntered == ""  {
                SVProgressHUD.showError(withStatus: "Please enter bank name to continue")
            }else{
                 SVProgressHUD.showError(withStatus: "Please enter account number to continue")
            }
        }else{
            //Hit APi to add
            guard let userID = UserStore.shared.user.userId else {
                return // Using this to remove optional value
            }
            let param = [
                "user_id":userID,
                "payment_type":2,
                "account_holder_name":accountHolderNameEntered,
                "bank_name":bank_nameEntered,
                "account_number":account_numberEntered
                ] as [String : Any]
            InfluencerApiStore2.sharedInstance.addPaymentMethodFromServer(param: param) { (success , _, _, _ ) in
                
                if success {
                    self.delegate?.didSuccessfullyAddedBankAccount()
                    self.dismiss(animated: true , completion: nil)
                }
                
            }
            
        }
    }
   

}
