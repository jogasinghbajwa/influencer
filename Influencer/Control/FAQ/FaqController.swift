//
//  FaqController.swift
//  Influencer
//
//  Created by iTz_saGGu on 03/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class FaqController: AbstractController {
    
    @IBOutlet weak var faqTableView: UITableView!
    var items = ["0","0","0","0"]
    var searchedItem = ""
    var allquestionsArray = [FAQModel]()
    
    @IBOutlet weak var txtfldSearch: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.faqTableView.register(FaqHeaderView.self, forHeaderFooterViewReuseIdentifier:"FaqHeaderView")
        addBackButton()
        txtfldSearch.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        getFAQFromServer()
    }
    
    @objc func textFieldDidChange(textField : UITextField){
        print(textField.text)
        searchedItem = unwrapString(string: textField.text)
        getFAQFromServer()
    }
    
    func getFAQFromServer(){
        let param = [
            "request_type":"faq",
            "search":searchedItem
        ]
        InfluencerApiStore2.sharedInstance.getFAQFromServer(param: param) { (success , questions ) in
            
            if  success {
                self.allquestionsArray = []
                if let questionArr = questions {
                    for item in questionArr {
                        self.allquestionsArray.append(item)
                    }
                }
                self.faqTableView.reloadData()
            }
        }
    }
    
}
//MARK:- TableView delegates & dataSources
extension FaqController:UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return allquestionsArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if allquestionsArray[section].isSelected {
            return 1
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FaqTableCell.reuseId) as! FaqTableCell
        cell.txtLabel.text = allquestionsArray[indexPath.section].answer
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FaqHeaderView") as! FaqHeaderView
        header.headerButton.tag = section
        header.titleLabel.text = allquestionsArray[section].question
        header.arrowImageView.image = allquestionsArray[section].isSelected == true ? #imageLiteral(resourceName: "arrow-collpase") : #imageLiteral(resourceName: "arrow-expand")
        header.headerButton.addTarget(self, action: #selector(headerDidSelect(btn:)), for: .touchUpInside)
        return header
    }
    
    @objc func headerDidSelect(btn:UIButton) {
     
        for item in allquestionsArray {
            item.isSelected = false
        }
        allquestionsArray[btn.tag].isSelected = true
        self.faqTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}

