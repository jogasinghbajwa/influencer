//
//  AboutController.swift
//  Influencer
//
//  Created by iTz_saGGu on 03/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class AboutController: AbstractController {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UITextView!
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
        getAboutUS()
        // Do any additional setup after loading the view.
    }
    
    func updateUI(aboutUsModel: AboutUsModel) {
        titleLabel.text = aboutUsModel.title
        descriptionLabel.text = aboutUsModel.content
    }
    
    //MARK:- Api method
    func getAboutUS() {
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param =  [
            ParameterNames.request_type.rawValue: "about_us"
            ] as [String : Any]
        InfluencerApiStore.sharedInstance.getAboutUS(param: param) { (success, aboutUsModel) in
            if success {
                if let aboutUsModel = aboutUsModel {
                    DispatchQueue.main.async {
                        self.updateUI(aboutUsModel: aboutUsModel)
                    }
                }
            }
        }
    }

}

//MARK:- TableView delegates & dataSources
extension AboutController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AboutTableCell.reuseId) as! AboutTableCell
        return cell;
    }
    
}
