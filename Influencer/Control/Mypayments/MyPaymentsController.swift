//
//  MyPaymentsController.swift
//  Influencer
//
//  Created by iTz_saGGu on 05/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit


class MyPaymentsController: AbstractController {
    @IBOutlet weak var paymentsTableView: UITableView!
   // @IBOutlet weak var vwTotalView:MBCircularProgressBarView!
    var allOldPaymentsArr = [MyPaymentModel]()
    @IBOutlet weak var vwTotalAmount: MBCircularProgressBarView!
    @IBOutlet weak var vwWeekPayment: MBCircularProgressBarView!
    
    
    //compaigns
    var sortMethod = "1"
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
        getPaymentDetailFromServer()
    }
    
    func getPaymentDetailFromServer(){
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param =  [
            ParameterNames.user_id.rawValue: userID,
            ParameterNames.sort.rawValue: sortMethod
            ] as [String : Any]
        
        InfluencerApiStore2.sharedInstance.getPaymentsListFromServer(param: param) { (success, payments, weekPayment,totalPayment ) in
            
            self.allOldPaymentsArr = []
            if success {
                
                if let paymentArr = payments {
                    for item in paymentArr {
                        self.allOldPaymentsArr.append(item)
                    }
                }
                self.paymentsTableView.reloadData()
                self.vwTotalAmount.value = CGFloat(integerLiteral: totalPayment ?? 0)
                self.vwWeekPayment.value = CGFloat(integerLiteral: weekPayment ?? 0)
            }
        }
    }


}

extension MyPaymentsController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allOldPaymentsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let paycell = tableView.dequeueReusableCell(withIdentifier: PaymentTableCell.reuseId) as! PaymentTableCell
        
        paycell.lblTitle.text = allOldPaymentsArr[indexPath.row].campaignName
        paycell.lblAmount.text = "$ " + allOldPaymentsArr[indexPath.row].amountEarned
//        paycell.lblDate.text = ""
        paycell.selectionStyle = .none
        return paycell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}
