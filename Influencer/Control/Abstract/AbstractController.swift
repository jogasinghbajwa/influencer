//
//  AbstractControl.swift
//  Influencer
//
//  Created by iTz_saGGu on 31/08/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class AbstractController: UIViewController {

    var model: AnyObject!
    var isControlVisible:Bool = true

    //MARK:- Get control from storyboard.
    class var control: AbstractController {
        return UIStoryboard.main.instantiateViewController(withIdentifier: String(describing: self)) as! AbstractController
    }
    
    class func controlWithModel(_ model: AnyObject) -> AbstractController {
        let control = self.control
        control.model = model
        return control
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
        
       addGradientInNavbar()
        setNavbarTitle()
        
    }
//    MARK: INIT
    func initViews() {
        
    }
    
//    MARK: TITLE
    func setNavbarTitle() {
        
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 18)!]
    }
    
    func addBackButton() {
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "back"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        btn1.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setLeftBarButtonItems([item1], animated: true)
        
    }
    
    func setUpRightMenu() {
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "sidemenu"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        btn1.addTarget(self, action: #selector(menuButtonClicked), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        self.mm_drawerController.openDrawerGestureModeMask = .custom
    }
    
    @objc func menuButtonClicked() {
        self.mm_drawerController.toggle(.right, animated: true, completion: nil)

    }
    
    @objc func backButtonClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //    MARK: NAVBAR GRADIENT METHODS
    func addGradientInNavbar() {
        if let navigationBar = self.navigationController?.navigationBar {
            let gradient = CAGradientLayer()
            var bounds = navigationBar.bounds
            bounds.size.height += UIApplication.shared.statusBarFrame.size.height
            gradient.frame = bounds
            gradient.colors =  [NAV_GRADINET_BLUE.cgColor,NAV_GRADINET_RED.cgColor]
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 0)
            
            if let image = getImageFrom(gradientLayer: gradient) {
                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            }
        }
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    
    

    

}
