//
//  WebViewController.swift
//  Influencer
//
//  Created by Joga Singh on 04/12/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit
import WebKit

protocol WebViewControllerDelegate {
    func instaAccessToken(token: String)
}
class WebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var webView: WKWebView!
    @IBOutlet var instagramWebView: UIWebView!
    var delegate: WebViewControllerDelegate?
    
    let INSTAGRAM_CLIENT_ID  = "47b17b33281047718cd5a3808e063186"
    let INSTAGRAM_CLIENTSERCRET = "4776adad7d76496c90c92c808bb079d5"
    let INSTAGRAM_REDIRECT_URI = "http://www.werkabee.com"


    let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    let INSTAGRAM_ACCESS_TOKEN  = "access_token"
    //let INSTAGRAM_SCOPE  = "likes+comments+relationships"
    let INSTAGRAM_SCOPE  = "basic"

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        let urlString = "https://api.instagram.com/oauth/authorize/?client_id=a8be2ad601824b548759963c347ec5f0&redirect_uri=http://google.com&response_type=token"
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        let urlString = "https://api.instagram.com/oauth/authorize/?client_id=a8be2ad601824b548759963c347ec5f0&redirect_uri=http://google.com&response_type=token"
        
        let urlString = "https://instagram.com/oauth/authorize/?client_id=a8be2ad601824b548759963c347ec5f0&redirect_uri=http://google.com&response_type=token&scope=basic"
        let newString = "https://api.instagram.com/oauth/authorize?app_id=433315670715556&redirect_uri=https://myprojectdemonstration.com/development/influencernew/&scope=user_profile,user_media&response_type=code"
        
        guard let value = (newString as NSString).addingPercentEncoding(withAllowedCharacters: .urlUserAllowed), let url = URL(string: newString) else {
            return
        }
//        webView.load(URLRequest(url: url))
        loadInstaLogin()
    }
    
    func loadInstaLogin() {
        let authURL = "\(INSTAGRAM_AUTHURL)?client_id=\(INSTAGRAM_CLIENT_ID)&redirect_uri=\(INSTAGRAM_REDIRECT_URI)&response_type=code&scope=\(INSTAGRAM_SCOPE)&DEBUG=True"
        instagramWebView?.loadRequest(URLRequest(url: URL(string: authURL)!))
        instagramWebView?.delegate = self
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UIWebView Delegate Method.
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        return self.checkRequest(forCallbackURL: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        
        instagramWebView?.layer.removeAllAnimations()
        instagramWebView?.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.1, animations: {() -> Void in
            
        })
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        
        instagramWebView?.layer.removeAllAnimations()
        instagramWebView?.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.1, animations: {() -> Void in
            
        })
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.webViewDidFinishLoad(webView)
    }
    
    func checkRequest(forCallbackURL request: URLRequest) -> Bool {
        let urlString: NSString = request.url!.absoluteString as NSString
        if urlString.hasPrefix(INSTAGRAM_REDIRECT_URI) {
                        // extract and handle access token
                        let range: NSRange = (urlString as NSString).range(of: "code=")
                        let token = ((urlString as NSString).substring(from: range.location + range.length))
                        delegate?.instaAccessToken(token: token)
                        print(token, " tokenVal")
            self.navigationController?.popViewController(animated: false)
        //                self.handleAuth(token)
                        return false
                    }
        
        return true
    }
    
    func makePostRequest(_ code: String) {
    
        let post: String = "client_id=\(INSTAGRAM_CLIENT_ID)&client_secret=\(INSTAGRAM_CLIENTSERCRET)&grant_type=authorization_code&redirect_uri=\(INSTAGRAM_REDIRECT_URI)&code=\(code)"
        let postData: Data? = post.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let postLength: String? = "\(UInt((postData?.count)!))"
        let requestData = NSMutableURLRequest(url: URL(string: "https://api.instagram.com/oauth/access_token")!)
        requestData.httpMethod = "POST"
        requestData.setValue(postLength, forHTTPHeaderField: "Content-Length")
        requestData.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        requestData.httpBody = postData
        
        
        let session = URLSession.shared
       
        let task = session.dataTask(with: requestData as URLRequest, completionHandler: {data, response, requestError -> Void in
            
            let reply = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            if let data = reply?.data(using: String.Encoding.utf8.rawValue) {
                
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                    print(json)
                    
                    let dict: [String: Any] = json.value(forKey: "user") as! [String : Any]
                    self.handleAuth(dict["username"] as! String)
                    
                } catch {
                    print("Something went wrong")
                }
            }
        })
        
        task.resume()
        
        
    }

    func handleAuth(_ authToken: String) {

        print("successfully logged in with Tocken == \(authToken)")
        
    }
}
