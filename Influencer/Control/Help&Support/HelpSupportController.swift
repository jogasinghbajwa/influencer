//
//  HelpSupportController.swift
//  Influencer
//
//  Created by iTz_saGGu on 04/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit
import MapKit
class HelpSupportController: AbstractController {
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var lblEmail: UITextField!
    @IBOutlet weak var lblPhoneNumber: UITextField!
    @IBOutlet weak var lblAddress: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
        getHelpDetail()
        // Do any additional setup after loading the view.
    }
    
    func getHelpDetail(){
        let param = [
            "request_type":"help"
        ]
        
        InfluencerApiStore2.sharedInstance.getHelpFromServer(param: param) { (success , help ) in
            
            if success {
                
                if let helpData = help {
                    self.lblEmail.text = helpData.email
                    self.lblPhoneNumber.text = helpData.phone
                    self.lblAddress.text = helpData.address
                    
//                    let location = locations.last as! CLLocation
//                    let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//                    var region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
//                    region.center = mapView.userLocation.coordinate
//                    mapView.setRegion(region, animated: true)
                    let latitude = Int(unwrapString(string:helpData.latitude ))
                    let longitude = Int(unwrapString(string:helpData.longitude ))
                    let location = CLLocation(latitude: CLLocationDegrees(latitude ?? 0), longitude: CLLocationDegrees(longitude ?? 0))
                    guard let lat = helpData.latitude, let logn = helpData.longitude else {
                        return
                    }
                    
                    let center = CLLocationCoordinate2D(latitude: (lat as NSString).doubleValue, longitude: (logn as NSString).doubleValue)
                    var region = MKCoordinateRegion(center: center , latitudinalMeters: 0.1, longitudinalMeters: 0.1)
                    region.center = center
                    self.mapView.setRegion(region, animated: true)
                }
                
            }
            
        }
        
    }

    @IBAction func helpButtonClicked(_ sender: Any) {
        self.navigationController?.pushViewController(ChatController.control, animated: true)
    }
}
