//
//  CampaignDetailController.swift
//  Influencer
//
//  Created by iTz_saGGu on 05/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class CampaignDetailController: AbstractController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var acceptButton: UIButton!
    @IBOutlet var rejectButton: UIButton!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var campaignNameLabel: UILabel!
    
    var campaignID: Int?
    var campaignDetail: CampaignDetail?
    
    @IBOutlet var commentTextField: UITextField!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet var activeStatusView: UIView!
    @IBOutlet var inactiveStatusView: UIView!
    @IBOutlet var categoryLabel: UILabel!
    
    var items = [["title":"Campaign detail","desc":"Music"],["title":"Type of post influencer","desc":"Dance"],["title":"Deal Code","desc":""],["title":"Description","desc":"Test description is written"]]
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
        self.navigationItem.title = "Dummy Campaign"
        // Do any additional setup after loading the view.
        if let campaignID = self.campaignID {
            getCampaignDetail(campaignID: campaignID)
        }
    }
    
    @IBAction func submitButtonClicked(_ sender: UIButton) {
        if let comment = commentTextField.text {
            if comment.count > 0 {
                postComment(comment: comment, categoryID: campaignDetail?.categoryId ?? 0)
            }
        }
    }
    
    @IBAction func acceptCampaignClicked(_ sender: UIButton) {
        acceptOrRejectCampaign(isAccepted: true)
    }
    
    @IBAction func rejectCampaignClicked(_ sender: UIButton) {
        acceptOrRejectCampaign(isAccepted: false)
    }
    
    
    func acceptOrRejectCampaign(isAccepted: Bool) {
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param =  [
            ParameterNames.user_id.rawValue: 01,
            ParameterNames.campaign_id.rawValue: campaignID ?? 0,
//            ParameterNames.category_id.rawValue: categoryID,
//            ParameterNames.comment.rawValue: comment
            ] as [String : Any]
        InfluencerApiStore.sharedInstance.acceptOrRejectCampaign(param: param, isAccept: isAccepted) { (success) in
            if(success) {
                DispatchQueue.main.async {
                    let button = isAccepted ? self.acceptButton : self.rejectButton
                    button?.isHidden = true
                }
            }
        }
    }
        
//        InfluencerApiStore.sharedInstance.postComment(param: param) { (success) in
//            if(success) {
//                self.commentTextField.text = ""
//                UIAlertController.showAlert(APP_NAME, message: "Your comment has been sent.", buttons: ["OK"]) { (aler, inde) in
//
//                }
//            }
//        }
//    }
    
    
    //MARK:- Update User Interface
    func updateUI(campaignDetail: CampaignDetail) {
        print(campaignDetail)
        headerImageView.setImage(urlString: IMAGE_BASE_URL + campaignDetail.image)
        campaignNameLabel.text = campaignDetail.campaignName
        self.navigationItem.title = campaignDetail.campaignName
        let startDate = campaignDetail.startDate.date("dd-MM-yyyy").string("MMM dd")
        let endDate = campaignDetail.endDate.date("dd-MM-yyyy")
         let endDateString = endDate.string("MMM dd")
        let year = endDate.string("yy")
//        dateLabel.text = dateForm.string("MMM dd, yyyy")
//        let startDate = campaignDetail.startDate.date("DD-MM-YY").string("MMM DD")
//        let endDate = campaignDetail.endDate.date("DD-MM-YY")
//        let endDateString = endDate.string("MMM DD")
//        let year = endDate.string("yy")
        dateLabel.text = "\(startDate) - \(endDateString), \(year)"
        categoryLabel.text = campaignDetail.category
        let status = campaignDetail.status.int == 1
        activeStatusView.isHidden = !status
        inactiveStatusView.isHidden = status
        priceLabel.text = "$" + campaignDetail.price
//        var endDateString = campaignDetail.startDate
//
//        let formatter = DateFormatter()
//        formatter.dateFormat = "DD-MM-YYYY"
//
//        let startDate
        self.campaignDetail = campaignDetail
        
        items = [["title":"Campaign detail","desc":campaignDetail.campaignTypeName],["title":"Type of post influencer","desc":campaignDetail.postType],["title":"Deal Code","desc": campaignDetail.dealCode],["title":"Description","desc":campaignDetail.descriptionField]]
        
        tableView.reloadData()
    }
    
    //MARK:- Api method
    func getCampaignDetail(campaignID: Int) {
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param =  [
            ParameterNames.user_id.rawValue: userID,
            ParameterNames.campaign_id.rawValue: campaignID
            ] as [String : Any]
        InfluencerApiStore.sharedInstance.getCampaignDetail(param: param) { (sucess, campaignDetail) in
            if(sucess) {
                guard let campaignDetail = campaignDetail else {
                    return
                }
                DispatchQueue.main.async {
                    self.updateUI(campaignDetail: campaignDetail)
                }
            }
        }
    }
    
    func postComment(comment: String, categoryID: Int) {
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param =  [
            ParameterNames.user_id.rawValue:userID,
            ParameterNames.campaign_id.rawValue: campaignID ?? 0,
            ParameterNames.category_id.rawValue: categoryID,
            ParameterNames.comment.rawValue: comment
            ] as [String : Any]
        InfluencerApiStore.sharedInstance.postComment(param: param) { (success) in
            if(success) {
                self.commentTextField.text = ""
                UIAlertController.showAlert(APP_NAME, message: "Your comment has been sent.", buttons: ["OK"]) { (aler, inde) in
                    
                }
            }
        }
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        self.headerImageView.roundCorners(corners: .bottomLeft, radius: 50
        )
    }

}

extension CampaignDetailController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
//        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CampaignDetailTableCell.reuseId) as! CampaignDetailTableCell
        cell.setUI(campaignDetail: campaignDetail)
        if indexPath.row == 2 {
            cell.grabButton.isHidden = false
            cell.grabButton.setTitle(items[indexPath.row]["desc"], for: .normal)
        } else {
            cell.grabButton.isHidden = true
        }
        cell.cellTitleLabel.text = items[indexPath.row]["title"]
        cell.celltextField.text = items[indexPath.row]["desc"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
