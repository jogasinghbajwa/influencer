//
//  SettingsController.swift
//  Influencer
//
//  Created by iTz_saGGu on 31/08/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKShareKit
import TwitterKit

class SettingsController: AbstractController {
    @IBOutlet var tableHeaderView: UIView!
    @IBOutlet weak var settingsTableView: UITableView!
    var isNotificationEnabled: Bool = false
    @IBOutlet weak var headerTitleLabel: UILabel!
    var socialArray=[["title":"Facebook","status":"0","image":#imageLiteral(resourceName: "facebook-active")],["title":"Instagram","status":"0","image":#imageLiteral(resourceName: "insta-inactive")],["title":"Twitter","status":"0","image":#imageLiteral(resourceName: "tw-active")],["title":"Snapchat","status":"0","image":#imageLiteral(resourceName: "snapchat-active")]]
    
    let settingsArray=["Notifications","About","FAQ"]
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveMenuIndex(_:)), name: NSNotification.Name(rawValue: "indexClick"), object: nil)
        setUpRightMenu()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getSocialMediaLinkStatus()
        getCurrentUserProfileDetail()
        self.isControlVisible = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isControlVisible = false
    }
    
    @objc func receiveMenuIndex(_ notification: NSNotification)  {
        if self.isControlVisible == true {
            let dict = notification.userInfo
            var selectedControl:UIViewController = UIViewController()
            let index = dict!["index"]! as! Int
            switch index {
            case 0:
                selectedControl = MyPaymentsController.control
                break
            case 1:
                selectedControl = HelpSupportController.control
                break
            case 2:
                self.tabBarController?.selectedIndex = 2
                self.mm_drawerController.closeDrawer(animated: true, completion: nil)
                return
            case 3:
                selectedControl = FaqController.control
                break
            case 4:
                selectedControl = AboutController.control
                break
                
            default:
                break
            }
            self.navigationController?.pushViewController(selectedControl, animated: true)
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        }
    }
    
    //MARK:- API Methods
    func enableOrDisableNotification(isEnable: Bool) {
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param =  [
            ParameterNames.user_id.rawValue: userID,
            ParameterNames.is_notifications_enabled.rawValue: isEnable ? 1 : 0,
            ] as [String : Any]
        
        InfluencerApiStore.sharedInstance.enableOrDisableNotifications(param: param) { (success) in
//            if(success) {
//                let onOff = isEnable == true ? "Off" : "On"
//                let message = "notifications turn \(onOff) successfully."
//                if success {
//                    let alert = UIAlertController(title: "Success", message: message , preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: .default , handler: { (_ ) in
//                        
//                    }))
//                    self.present(alert , animated: true , completion: nil)
//                }
//                
//            }
        }
    }
    
    func getSocialMediaLinkStatus() {
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param =  [
            ParameterNames.user_id.rawValue: userID,
            ] as [String : Any]
        
        InfluencerApiStore.sharedInstance.getSocialMediaLinkStaus(param: param) { (success, socialStatus) in
            if(success) {
                self.socialArray=[["title":"Facebook","status":"\(socialStatus?.fbStatus ?? 0)","image":#imageLiteral(resourceName: "facebook-active")],["title":"Instagram","status":"\(socialStatus?.instagramStatus ?? 0)","image":#imageLiteral(resourceName: "insta-inactive")],["title":"Twitter","status":"\(socialStatus?.twitterStatus ?? 0)","image":#imageLiteral(resourceName: "tw-active")],["title":"Snapchat","status":"\(socialStatus?.snapchatStatus ?? 0)","image":#imageLiteral(resourceName: "snapchat-active")]]
                DispatchQueue.main.async {
                    self.settingsTableView.reloadData()
                }
                
            }
        }
    }
    
    func getCurrentUserProfileDetail(){
            guard let userID = UserStore.shared.user.userId else {
                return // Using this to remove optional value
            }
            let param = [
                "user_id":userID
            ]
            print(param)
            InfluencerApiStore.sharedInstance.requestUserProfileDetail(param: param) { (success , userDetail) in
                
                if success {
                    
    //                0 . "First name"
    //                1.  "Last name"
    //                2.  "Gender"
    //                3.  "Address"
    //                4.  "Country"
    //                5.  "City"
    //                6.  "Contact"
    //                7.  "Email address"
    //                8.  "DOB"
                    self.isNotificationEnabled = userDetail?.isNotificationsEnabled ?? false
                    DispatchQueue.main.async {
                        self.settingsTableView.reloadData()
                    }
                }
                
                
            }
            
        }
    
    func InstagramSocialLoginSelected() {
        let webVC: WebViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        webVC.delegate = self
        self.navigationController?.pushViewController(webVC, animated: true)
    }
}

extension SettingsController: WebViewControllerDelegate {
    func instaAccessToken(token: String) {
        print(token)
        self.addSocialMediaAccount(withtoken: token, type: 2)
    }
    
    
}

//MARK:- TableView delegates & dataSources
extension SettingsController:UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? socialArray.count : settingsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingSocialTableCell.reuseId) as! SettingSocialTableCell
            let indexItem = socialArray[indexPath.row]
            cell.imagIcon.image = indexItem["image"] as? UIImage
            cell.socialTitleLabel.text =  indexItem["title"] as? String
            let status = indexItem["status"] as? String
            if status == "1" {
                cell.statusImageView.image = #imageLiteral(resourceName: "check-green")
                cell.statusImageViewWidth.constant = 20
            } else {
                cell.statusImageView.image = #imageLiteral(resourceName: "link")
                cell.statusImageViewWidth.constant = 30
            }
            return cell;
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingTableCell.reuseId) as! SettingTableCell
            if indexPath.row == 0  {
                cell.switchButton.isHidden = false
                cell.arrowImageView.isHidden = true
            } else {
                cell.switchButton.isHidden = true
                cell.arrowImageView.isHidden = false
            }
            cell.switchButton.setOn(isNotificationEnabled, animated: true)
            
            let indexItem = settingsArray[indexPath.row]
            if indexPath.row == 0 {
                cell.switchButton.addTarget(self , action: #selector(notificationSwitchPressed(sender:)), for: .valueChanged)
            }
            cell.settingTitleLabel.text = indexItem
            return cell;
        }
    }
    
    @objc func notificationSwitchPressed(sender:UISwitch) {
        //enableOrDisableNotification
        print(sender.isOn)
        self.enableOrDisableNotification(isEnable: sender.isOn)
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            self.headerTitleLabel.text = "Click to link the social profiles"
            return self.tableHeaderView
        } else {
            self.headerTitleLabel.text = "Enable or disable the notifications in app"
            return self.tableHeaderView
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                
                let loginManager = LoginManager()
                loginManager.logIn(permissions: ["public_profile","email", "user_birthday", "user_hometown", "user_gender", "user_photos", "instagram_basic"] , from:  self) { (result , error ) in
                    
                    guard let result = result else {
                        print((error?.localizedDescription)!)
                        return
                    }
                    if(result.isCancelled) {
                        print("Cancelled")
                        //     showToast(text: "Login process cancelled by user.")
                        return
                    }
                    print(result.grantedPermissions)
                    print(result.declinedPermissions)
                    print(result.token?.tokenString)
                    print(result)
                    self.addSocialMediaAccount(withtoken: result.token?.tokenString, type: 1 )
                }
                
            } else if indexPath.row == 1 {
                InstagramSocialLoginSelected()
            }else if indexPath.row == 2 {
                TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
                    if (session != nil) {
                        print("signed in as \(session?.userName)");
                        if let token = session?.authToken {
                            self.addSocialMediaAccount(withtoken: token, type: 3)
                        }
                    } else {
                        print("error: \(error?.localizedDescription)");
                    }
                })
            }
        } else {
            if indexPath.row == 1 {
                print("About Us")
                let aboutUs: AboutController = AboutController.control as! AboutController
                self.navigationController?.pushViewController(aboutUs, animated: true)
            } else if indexPath.row == 2 {
                print("FAQ")
                let aboutUs: FaqController = FaqController.control as! FaqController
                self.navigationController?.pushViewController(aboutUs, animated: true)
                
            }
        }
        
    }
    
    func addSocialMediaAccount(withtoken:String?, type: Int) {
        
        if let token = withtoken  {
            // hit API to add fb account.
            guard let userID = UserStore.shared.user.userId else {
                return // Using this to remove optional value
            }
            let param = [
                "user_id":userID,
                "account_type": type,
                "token":token
                
                ] as [String : Any]
            InfluencerApiStore2.sharedInstance.addSocialMediaAccount(param: param) { (success ) in
                
                if success {
                    let alert = UIAlertController(title: "success".uppercased() , message: "Account Updated successfully", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_ ) in
                        
                    }))
                    self.present(alert , animated: true , completion: nil)
                    
//                    if (type == 1) {
                        self.socialArray[type - 1]["status"] = "1"
                        DispatchQueue.main.async {
                            self.settingsTableView.reloadData()
                        }
                        
//                    }
                }
            }
          
            
        }
        
    }
    
    
}
