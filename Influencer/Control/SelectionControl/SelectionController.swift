//
//  SelectionController.swift
//  Influencer
//
//  Created by iTz_saGGu on 28/10/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class SelectionController: AbstractController {
    @IBOutlet weak var tableView: UITableView!
    
    var pickedValueCallback : (([String:Any]) -> ())?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backgroundButton: UIButton!
    var isForCountry = false
    var countryList:[Country] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        view.isOpaque = false
        view.backgroundColor = .clear
        if isForCountry {
            self.titleLabel.text = "Select your country"
            getAllCountries()
        } else {
            self.titleLabel.text = "Select your state"
        }
        // Do any additional setup after loading the view.
    }
    
    private  func getAllCountries() {
        InfluencerApiStore.sharedInstance.requestGetAllCountries(param: ["countries":"all"]) { (success, countries) in
            self.countryList = countries
            self.tableView.reloadData()
        }
    }
    private  func getAllStatesWithCountryId(id:String) {
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.backgroundButton.backgroundColor = UIColor.black
        self.backgroundButton.alpha = 0.3
    }
    
    @IBAction func crossClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func backgroundButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension SelectionController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = self.countryList[indexPath.row].countryName
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pickedValueCallback?(self.countryList[indexPath.row].toDictionary())
        self.dismiss(animated: true, completion: nil)
    }
}
