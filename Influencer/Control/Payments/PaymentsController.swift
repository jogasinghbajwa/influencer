//
//  PaymentsController.swift
//  Influencer
//
//  Created by iTz_saGGu on 01/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class PaymentsController: AbstractController {

    @IBOutlet weak var paymentTableView: UITableView!
    
    var myPaymentArr = [PaymentMethodModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentTableView.delegate = self
        paymentTableView.dataSource = self
        addBackButton()
        getOldPaymentMethods()
        self.navigationItem.title = "Payment Account"
        
    }
    
    @IBAction func addPaymentClicked(_ sender: Any) {

        
        let alert = UIAlertController(title: nil , message: "Choose Payment Model" , preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Paypal", style: .default , handler: { (_ ) in
            let tabbarControl = UIStoryboard.main.instantiateViewController(withIdentifier: "AddPaypalController") as! AddPaypalController
            tabbarControl.delegate = self
            self.present(tabbarControl, animated: true , completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Bank Account", style: .default , handler: { (_ ) in
            //AddBankAccountViewController
            let tabbarControl = UIStoryboard.main.instantiateViewController(withIdentifier: "AddBankAccountViewController") as! AddBankAccountViewController
            tabbarControl.delegate = self
            self.present(tabbarControl, animated: true , completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler: { (_ ) in
            
        }))
        self.present(alert , animated: true , completion: nil)
    }
    
    func getOldPaymentMethods(){
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param = [
            "user_id":userID
        ]
        InfluencerApiStore2.sharedInstance.getPaymentDetails(param: param) { (success , paymentArr ) in
              self.myPaymentArr = []
            if success {
                self.myPaymentArr = []
                if let arr = paymentArr {
                    for item in arr {
                      if  self.myPaymentArr.contains(item) {
                            
                      }else{
                      self.myPaymentArr.append(item)
                        }
                    }
                }
            }
            self.paymentTableView.reloadData()
        }
        
    }
    
    func deletePaymentMethods(methodID: String) {
        
        let param =  [ParameterNames.payment_method_id.rawValue: methodID] as [String : Any]

        InfluencerApiStore.sharedInstance.deletePaymentMethod(param: param) { (success) in
            if success {
                DispatchQueue.main.async {
                    self.getOldPaymentMethods()
                }
            }
        }
    }
}

extension PaymentsController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return myPaymentArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        let cell: PaymentsCell = tableView.dequeueReusableCell(withIdentifier: "PaymentsCell", for: indexPath) as! PaymentsCell
        cell.payment = self.myPaymentArr[indexPath.row]
        cell.cellIndex = indexPath.row
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
        
    }
    
    
}

extension PaymentsController : PaymentsCellDelegate {
    func deletePaymentMethodSelected(index: Int?) {
        guard let index = index else { return }
        deletePaymentMethods(methodID: self.myPaymentArr[index].id)
    }
    
    
}

extension PaymentsController : AddPaypalDelegate {
    
    func didSuccessfullyAddedAccount() {
        getOldPaymentMethods()
    }

}

extension PaymentsController : AddBankAccountDelegate {
    
    func didSuccessfullyAddedBankAccount() {
        getOldPaymentMethods()
    }
    
}


//PaymentMethodModel
