//
//  AddPaypalController.swift
//  Influencer
//
//  Created by iTz_saGGu on 01/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

protocol AddPaypalDelegate {
    
    func didSuccessfullyAddedAccount()
}

class AddPaypalController: AbstractController {
   
    var delegate : AddPaypalDelegate?
    
    @IBOutlet weak var txtfldTitle: UITextField!
    @IBOutlet weak var txtfldEmailId: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.isOpaque = false
        view.backgroundColor = .clear
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     
    }
    
    @IBAction func crossClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func saveBtnPressed(_ sender: Any) {
        let titleEntered = unwrapString(string: txtfldTitle.text)
        let emailEntered = unwrapString(string: txtfldEmailId.text)
        
        if titleEntered == "" || emailEntered == "" {
            if titleEntered == "" {
                SVProgressHUD.showError(withStatus: "Please enter title to continue")
            }else{
                SVProgressHUD.showError(withStatus: "Please enter email id to continue")
            }
        }else{
            //Hit APi to add
            guard let userID = UserStore.shared.user.userId else {
                return // Using this to remove optional value
            }
            let param = [
                "user_id":userID,
                "payment_type":1,
                "title":titleEntered,
                "email_id":emailEntered
                ] as [String : Any]
            InfluencerApiStore2.sharedInstance.addPaymentMethodFromServer(param: param) { (success , _, _, _ ) in
                
                if success {
                    self.delegate?.didSuccessfullyAddedAccount()
                    self.dismiss(animated: true , completion: nil)
                }
                
            }
            
        }
    }
 
    
}
