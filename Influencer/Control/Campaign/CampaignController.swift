//
//  CampaignController.swift
//  Influencer
//
//  Created by iTz_saGGu on 31/08/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class CampaignController: AbstractController {
    @IBOutlet weak var allLine: UIView!
    
    @IBOutlet weak var rejectLine: UIView!
    @IBOutlet weak var approvedLine: UIView!
    @IBOutlet weak var newLine: UIView!
    @IBOutlet weak var campaignTableView: UITableView!
    var campaignList: [Campaign] = [] {
        didSet {
            campaignTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.campaignTableView.register(UINib.init(nibName: CampaignTableCell.reuseId, bundle: Bundle.main), forCellReuseIdentifier: CampaignTableCell.reuseId)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveMenuIndex(_:)), name: NSNotification.Name(rawValue: "indexClick"), object: nil)
        setUpRightMenu()
        hideLine()
        self.allLine.isHidden = false
        
        getCampaignList(order: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isControlVisible = true
        if !UserStore.shared.user.isProfileUpdated {
            self.tabBarController?.selectedIndex = 3
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isControlVisible = false
    }
    
    @objc func receiveMenuIndex(_ notification: NSNotification)  {
        if self.isControlVisible == true {
            let dict = notification.userInfo
            var selectedControl:UIViewController = UIViewController()
            let index = dict!["index"]! as! Int
            switch index {
            case 0:
                selectedControl = MyPaymentsController.control
                break
            case 1:
                selectedControl = HelpSupportController.control
                break
            case 2:
                self.tabBarController?.selectedIndex = 2
                self.mm_drawerController.closeDrawer(animated: true, completion: nil)
                return
                
            case 3:
                selectedControl = FaqController.control
                break
            case 4:
                selectedControl = AboutController.control
                break
                
            default:
                break
            }
            self.navigationController?.pushViewController(selectedControl, animated: true)
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        }
    }
    
    @IBAction func topHeaderButtonClicked(_ sender: UIButton) {
        hideLine()
        var orderID = 0
        switch sender.tag {
        case 1:
            self.allLine.isHidden = false
            orderID = 0
            break
        case 2:
            self.newLine.isHidden = false
            orderID = 1
            break
        case 3:
            self.approvedLine.isHidden = false
            orderID = 2
            break
        case 4:
            self.rejectLine.isHidden = false
            orderID = 3
            break
        default:
            break
        }
        getCampaignList(order: orderID)
    }
    
    func hideLine() {
        self.allLine.isHidden = true
        self.newLine.isHidden = true
        self.approvedLine.isHidden = true
        self.rejectLine.isHidden = true
    }
    
    //MARK:- Api method
    func getCampaignList(order: Int) {
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param =  [
            ParameterNames.user_id.rawValue: userID,
            ParameterNames.order.rawValue: order
            ] as [String : Any]
        InfluencerApiStore.sharedInstance.getCampaignList(param: param) { (success, CampaignList) in
            self.campaignList.removeAll()
            if success {
                if let list = CampaignList {
                    self.campaignList.append(contentsOf: list)
                }
            }
        }
    }
    
  

    
}

//MARK:- TableView delegates & dataSources
extension CampaignController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return campaignList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CampaignTableCell.reuseId) as! CampaignTableCell
        cell.setUI(campaign: campaignList[indexPath.row])
        return cell;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let campaignDetail: CampaignDetailController = CampaignDetailController.control as! CampaignDetailController
        campaignDetail.campaignID = campaignList[indexPath.row].campaignId
        self.navigationController?.pushViewController(campaignDetail, animated: true)
    }
}
