//
//  ProfileController.swift
//  Influencer
//
//  Created by iTz_saGGu on 31/08/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class ProfileController: AbstractController {
    @IBOutlet weak var userEmailButton: UILabel!
    @IBOutlet weak var headerExtImageView: UIImageView!
    
    @IBOutlet weak var userImageView: DesignableImageView!
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet weak var userNameLable: UILabel!
    @IBOutlet weak var headerExtenstionView: UIView!
    var countryId = ""
    var selectedImage:UIImage!
    var profileValues = [["title":"First name","value":""],["title":"Last name","value":""],["title":"Gender","value":""],
                         ["title":"Address","value":""],
                         ["title":"Country","value":""],["title":"City","value":""],["title":"Contact","value":""],["title":"Email address","value":""],["title":"DOB","value":""]
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        let gradient = CAGradientLayer()
        var bounds = self.headerExtenstionView.bounds
        bounds.size.height += UIApplication.shared.statusBarFrame.size.height
        gradient.frame = bounds
        gradient.colors =  [NAV_GRADINET_BLUE.cgColor,NAV_GRADINET_RED.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        
        if let image = getImageFrom(gradientLayer: gradient) {
            self.headerExtImageView.image = image
        }
        self.userImageView.actionBlock {
            self.userImageClicked()
        }
        setUpRightMenu()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveMenuIndex(_:)), name: NSNotification.Name(rawValue: "indexClick"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isControlVisible = true
        //        self.navigationController?.isNavigationBarHidden = true
        getCurrentUserProfileDetail()
    }
    
    func getCurrentUserProfileDetail(){
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param = [
            "user_id":userID
        ]
        print(param)
        InfluencerApiStore.sharedInstance.requestUserProfileDetail(param: param) { (success , userDetail) in
            print(userDetail?.toDictionary())
            if success {
                
//                0 . "First name"
//                1.  "Last name"
//                2.  "Gender"
//                3.  "Address"
//                4.  "Country"
//                5.  "City"
//                6.  "Contact"
//                7.  "Email address"
//                8.  "DOB"
                
                 self.profileValues[0]["value"] = unwrapString(string: userDetail?.firstName)
                 self.profileValues[1]["value"] = unwrapString(string: userDetail?.lastName)
                 self.profileValues[2]["value"] = unwrapString(string: userDetail?.gender)
                 self.profileValues[3]["value"] = unwrapString(string: userDetail?.address)
                 self.profileValues[4]["value"] = unwrapString(string: userDetail?.country)
                 self.profileValues[5]["value"] = unwrapString(string: userDetail?.city)
                 self.profileValues[6]["value"] = unwrapString(string: userDetail?.phone)
                 self.profileValues[7]["value"] = unwrapString(string: userDetail?.email)
                 self.profileValues[8]["value"] = unwrapString(string: userDetail?.dob)
                self.userImageView.setImage(urlString: userDetail?.profileImage)

                self.userNameLable.text = unwrapString(string: userDetail?.firstName) + unwrapString(string: userDetail?.lastName)
                self.userEmailButton.text = unwrapString(string: userDetail?.email)
                self.countryId = userDetail?.countryId ?? "0"
                self.profileTableView.reloadData()
            }
            
            
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.isControlVisible = false
        
    }
    
    @objc func receiveMenuIndex(_ notification: NSNotification)  {
        if self.isControlVisible == true {
            let dict = notification.userInfo
            var selectedControl:UIViewController = UIViewController()
            let index = dict!["index"]! as! Int
            switch index {
            case 0:
                selectedControl = MyPaymentsController.control
                break
            case 1:
                selectedControl = HelpSupportController.control
                break
            case 2:
                self.tabBarController?.selectedIndex = 2
                self.mm_drawerController.closeDrawer(animated: true, completion: nil)
                return
            case 3:
                selectedControl = FaqController.control
                break
            case 4:
                selectedControl = AboutController.control
                break
                
            default:
                break
            }
            self.navigationController?.pushViewController(selectedControl, animated: true)
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        }
    }
    
    //    MARK: Actions
    @IBAction func addPaymentButtonClicked(_ sender: Any) {
        self.navigationController?.pushViewController(PaymentsController.control, animated: true)
    }
    
    func userImageClicked() {
        ImagePickerManager().pickImage(self){ image in
            self.userImageView.image = image
        }
    }
    @IBAction func submitButtonClicked(_ sender: Any) {
        var firstName = ""
        var lastName = ""
        var gender = ""
        var address = ""
        var city = ""
        var phone = ""
        var dob = ""
        var couId = ""
        var tempEmail = ""
        for item in self.profileValues {
            if item["title"] == "Email address" {
                if item["value"]?.isValidEmail == false {
                    UIAlertController.showAlert(APP_NAME, message: "Enter a valid email address", buttons: ["OK"]) { (aler, inde) in
                        
                    }
                }
                break
            }
            
            if item["value"] == "" {
                let title  = item["title"]
                let alertMessage = "Please enter " + title!
                UIAlertController.showAlert(APP_NAME, message: alertMessage, buttons: ["OK"]) { (aler, inde) in
                    
                }
                break
            }
           
            
           
        }
        
        for item in self.profileValues {
            if item["title"] == "First name" {
                firstName = item["value"]!
            } else if item["title"] == "Last name" {
                lastName = item["value"]!
            }else if item["title"] == "Gender" {
                gender = item["value"]!
            }else if item["title"] == "Address" {
                address = item["value"]!
            } else if item["title"] == "City" {
                city = item["value"]!
            }  else if item["title"] == "Contact" {
                phone = item["value"]!
            } else  if item["title"] == "Email address" {
                tempEmail = item["value"]!
            } else  if item["title"] == "DOB" {
                dob = item["value"]!
            }
        }
        
        if self.userImageView.image == nil {
            UIAlertController.showAlert(APP_NAME, message: "Please add your profile image", buttons: ["OK"]) { (aler, inde) in
                
            }
            return
        }
        
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        
        let params = ["user_id":userID,
                      "first_name":firstName,
                      "last_name":lastName,
                      "gender":gender,
                      "address":address,
                      "city":city,
                      "phone":phone,
                      "dob":dob,
                      "profile_image":self.userImageView.image!,
                      "device_type":"2",
                      "country_id":countryId,
                      "temporary_email":tempEmail] as [String : Any];
        
        InfluencerApiStore.sharedInstance.requestUpdateUserProfile(param: params) { (success , user_Detail ) in
            if success {
            let alert = UIAlertController(title: "Success", message: "Profile detail updated successfully.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true , completion: nil)
            }
        }
        
    }
    
    
}

//MARK:- TableView delegates & dataSources
extension ProfileController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableCell.reuseId) as! ProfileTableCell
        
        cell.titleTopLabel.text = self.profileValues[indexPath.row]["title"]
        cell.cellTextField.placeholder = self.profileValues[indexPath.row]["title"]
        cell.cellTextField.text = self.profileValues[indexPath.row]["value"]
        cell.cellTextField.delegate = self
        cell.cellTextField.tag = indexPath.row
        let titleFromArr = self.profileValues[indexPath.row]["title"]
        if unwrapString(string: titleFromArr ) == "Email address" {
            cell.cellTextField.isUserInteractionEnabled = false
        }else{
            cell.cellTextField.isUserInteractionEnabled = true
        }
        if self.profileValues[indexPath.row]["title"] == "DOB" {
            self.createPickerView(sender: cell.cellTextField)
        } else {
            cell.cellTextField.inputView = nil
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func createPickerView(sender: UITextField){
        let datePickerView : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        sender.inputView = datePickerView
        datePickerView.tag = sender.tag
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged(caller:)), for: .valueChanged)
    }
    
    @objc func datePickerValueChanged(caller: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.profileValues[caller.tag]["value"] = dateFormatter.string(from: caller.date)
    }
    
    
}

extension ProfileController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let title  = self.profileValues[textField.tag]["title"]
        let control = SelectionController.control  as! SelectionController
        control.modalPresentationStyle = .overCurrentContext
        if title == "State" {
            control.isForCountry = false
            
            self.present(control, animated: true, completion: nil)
        } else if title == "Country" {
            control.pickedValueCallback = { value in
                print(value)
                self.profileValues[textField.tag]["value"] = value["country_name"] as? String ?? ""
                self.countryId = value["country_id"] as? String ?? ""
            }
            control.isForCountry = true
            self.present(control, animated: true, completion: nil)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        self.profileValues[textField.tag]["value"] = newString
        return true
    }
}
