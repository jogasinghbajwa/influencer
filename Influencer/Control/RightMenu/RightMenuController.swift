//
//  RightMenuController.swift
//  Influencer
//
//  Created by iTz_saGGu on 03/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class RightMenuController: AbstractController {
    @IBOutlet weak var menuTableView: UITableView!
    let menuItems = ["My Payments", "Help & Contact", "Setting", "FAQ", "About us"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func logoutButtonClicked(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: isUserLoggedIn)
        ActionShowLogin.execute()
    }
    
    func selectedIndex(index: Int) {
      
        NotificationCenter.default.post(name: .init("indexClick"), object: nil, userInfo: ["index" : index])
        
    }
    
    
}

//MARK:- TableView delegates & dataSources
extension RightMenuController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuTableCell.reuseId) as! MenuTableCell
        cell.menuTitleLabel.text = menuItems[indexPath.row]
        return cell;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex(index: indexPath.row)
    }
}
