//
//  ChatController.swift
//  Influencer
//
//  Created by iTz_saGGu on 04/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class ChatController: AbstractController {
    @IBOutlet weak var messageViewBottom: NSLayoutConstraint!
    @IBOutlet weak var placeHolder: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var chatTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
        let indexp = IndexPath.init(row: 0, section: 0)
        self.chatTableView.scrollToRow(at: indexp, at: .bottom, animated: false)
        
        
        
        
        // Do any additional setup after loading the view.
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            self.messageViewBottom.constant = (keyboardHeight - 44) - UIApplication.rootViewController.view.safeAreaInsets.bottom
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func sendButtonClicked(_ sender: Any) {
    }
}

extension ChatController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chatCell = tableView.dequeueReusableCell(withIdentifier: ChatTableCell.reuseId) as! ChatTableCell
        return chatCell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
}

extension ChatController:UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
            self.placeHolder.isHidden = true;
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.messageViewBottom.constant = 0
        self.view.layoutIfNeeded()
        if textView.text!.length > 0 {
            self.placeHolder.isHidden = true;
        } else {
            self.placeHolder.isHidden = false;
        }
        
    }
}
