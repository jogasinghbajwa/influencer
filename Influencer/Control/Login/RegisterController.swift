//
//  RegisterController.swift
//  Influencer
//
//  Created by iTz_saGGu on 31/08/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class RegisterController: AbstractController {

    @IBOutlet weak var backGroundImageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPassTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    //    MARK: INIT
    
    override func initViews() {
        self.backGroundImageView.image  = Images.Login
    }
    

    
    //    MARK: ACTIONS
    
    @IBAction func submitButtonClicked(_ sender: Any) {
        if !self.emailTextField.isValidEmail {
            UIAlertController.showAlert(APP_NAME, message: "Please enter a valid email address", buttons: ["Ok"]) { (alert, index) in
                
            }
        } else if self.passwordTextField.text!.isEmpty {
            UIAlertController.showAlert(APP_NAME, message: "Please enter a password", buttons: ["Ok"]) { (alert, index) in
                
            }
        } else if self.confirmPassTextField.text!.isEmpty {
            UIAlertController.showAlert(APP_NAME, message: "Please confirm your password", buttons: ["Ok"]) { (alert, index) in
                
            }
        } else if self.passwordTextField.text != self.confirmPassTextField.text {
            UIAlertController.showAlert(APP_NAME, message: "Password & confirm password are not same", buttons: ["Ok"]) { (alert, index) in
                
            }
        } else {
            
            let params  = [
                "username": self.emailTextField.text!,
                "device_type": 2,
                "password":self.passwordTextField.text!
                ] as [String : Any]
            
            
            InfluencerApiStore.sharedInstance.requestRegisterUser(param: params) { (success) in
                
            }
        }
    }
    
    @IBAction func crossButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
  

}
