//
//  LoginController.swift
//  Influencer
//
//  Created by iTz_saGGu on 31/08/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit
import TwitterKit

class LoginController: AbstractController {
    @IBOutlet weak var backGroundImageView: UIImageView!
    @IBOutlet weak var rememberButton: UIButton!
    var isRememberMe: Bool = true
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.emailTextField.text = "harmeet@mailinator.com"
//        self.passTextField.text = "Singh@221"
        
        self.emailTextField.text = "my_email11@gmail.com"
        self.passTextField.text = "google1234"
    }
    
    //    MARK: INIT
    
    override func initViews() {
        self.backGroundImageView.image  = Images.Login
    }
    
    //    MARK: ACTIONS
    
    @IBAction func submitButtonClicked(_ sender: Any) {
        
        
        if !self.emailTextField.isValidEmail {
            UIAlertController.showAlert(APP_NAME, message: "Please enter a valid email address", buttons: ["Ok"]) { (alert, index) in

            }
        } else if self.passTextField.text!.isEmpty {
            UIAlertController.showAlert(APP_NAME, message: "Please enter your password", buttons: ["Ok"]) { (alert, index) in

            }
        } else {
            let param =  ["username":self.emailTextField.text!,
                          "device_type":2,
                          "device_id":UIDevice.current.description,
                          "password":self.passTextField.text!,
                          "fcm_token":"qswertjkjh"] as [String : Any]
            InfluencerApiStore.sharedInstance.requestLoginUser(param: param) { (success) in
                if self.isRememberMe {
                    UserDefaults.standard.set(true, forKey: isUserLoggedIn)
                }
                if success {
                    ActionShowHome.execute()
                }
            }
        }
        
        //
    }
    
    @IBAction func remberButtonClicked(_ sender: Any) {
        if self.rememberButton.image(for: .normal) == #imageLiteral(resourceName: "uncheked") {
                isRememberMe = true
            self.rememberButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
        } else {
               isRememberMe = false
            self.rememberButton.setImage(#imageLiteral(resourceName: "uncheked"), for: .normal)
        }
    }
    
    @IBAction func registerButtonClicked(_ sender: Any) {
        self.present(RegisterController.control, animated: true, completion: nil)
    }
}
