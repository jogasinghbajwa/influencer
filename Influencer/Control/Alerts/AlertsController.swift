//
//  AlertsController.swift
//  Influencer
//
//  Created by iTz_saGGu on 31/08/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class AlertsController: AbstractController {
    @IBOutlet weak var alertTableView: UITableView!
    
    var alertList: [UserNotification] = [] {
        didSet {
            alertTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.alertTableView.register(UINib.init(nibName: AlertTableCell.reuseId, bundle: Bundle.main), forCellReuseIdentifier: AlertTableCell.reuseId)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveMenuIndex(_:)), name: NSNotification.Name(rawValue: "indexClick"), object: nil)
        setUpRightMenu()
        getNotificationList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isControlVisible = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isControlVisible = false
    }

    @objc func receiveMenuIndex(_ notification: NSNotification)  {
        if self.isControlVisible == true {
            let dict = notification.userInfo
            var selectedControl:UIViewController = UIViewController()
            let index = dict!["index"]! as! Int
            switch index {
            case 0:
                selectedControl = MyPaymentsController.control
                break
            case 1:
                selectedControl = HelpSupportController.control
                break
            case 2:
                self.tabBarController?.selectedIndex = 2
                self.mm_drawerController.closeDrawer(animated: true, completion: nil)
                return
            case 3:
                selectedControl = FaqController.control
                break
            case 4:
                selectedControl = AboutController.control
                break
                
            default:
                break
            }
            self.navigationController?.pushViewController(selectedControl, animated: true)
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        }
    }
    
    //MARK:- Api method
    func getNotificationList() {
        guard let userID = UserStore.shared.user.userId else {
            return // Using this to remove optional value
        }
        let param =  [
            ParameterNames.user_id.rawValue:userID,
            ] as [String : Any]
        InfluencerApiStore.sharedInstance.getAlertList(param: param) { (success, notificationList) in
        if success {
            self.alertList.removeAll()
            if let list = notificationList {
                    self.alertList.append(contentsOf: list)
                }
            }
        }
    }
}
//MARK:- TableView delegates & dataSources
extension AlertsController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return alertList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AlertTableCell.reuseId) as! AlertTableCell
        cell.setUI(alert: alertList[indexPath.row])
        return cell;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = MyPaymentsController.control
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
