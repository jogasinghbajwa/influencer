//
//  SettingSocialTableCell.swift
//  Influencer
//
//  Created by iTz_saGGu on 01/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class SettingSocialTableCell: AbstractTableCell {
    @IBOutlet weak var imagIcon: UIImageView!
    @IBOutlet weak var statusImageView: UIImageView!
    
    @IBOutlet weak var statusImageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var socialTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
