//
//  SettingTableCell.swift
//  Influencer
//
//  Created by iTz_saGGu on 01/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class SettingTableCell: AbstractTableCell {
    @IBOutlet weak var settingTitleLabel: UILabel!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var switchButton: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        switchButton.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
