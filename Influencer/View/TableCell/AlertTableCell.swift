//
//  AlertTableCell.swift
//  Influencer
//
//  Created by iTz_saGGu on 01/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class AlertTableCell: AbstractTableCell {

//    @IBOutlet var statusView: UIView!
//    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUI(alert: UserNotification) {
        setPreferences(name: alert.notificationName, date: alert.date, imageURL: alert.notificationImage)
       }
       
       private func setPreferences(name: String, date: String, imageURL: String) {
//           statusView.isHidden = !status
        let dateForm = date.date("yyyy-MM-dd HH:mm:ss")
        dateLabel.text = dateForm.string("MMM dd, yyyy")
           iconImageView.setImage(urlString: imageURL)
//           priceLabel.text = price
           nameLabel.text = name
       }
    
}
