//
//  PaymentsCell.swift
//  Influencer
//
//  Created by Manjodh Singh on 05/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

protocol PaymentsCellDelegate {
    func deletePaymentMethodSelected(index: Int?)
}
class PaymentsCell: UITableViewCell {

    @IBOutlet weak var lblAccountType : UILabel!
    @IBOutlet weak var lblPAypalIdorHolderNameHeading: UILabel!
    @IBOutlet weak var lblPAypalIdorHolderName: UILabel!
    @IBOutlet weak var lblEmailIdorAccountNumberHeading : UILabel!
    @IBOutlet weak var lblEmailIdorAccountNumber : UILabel!
    var delegate: PaymentsCellDelegate?
    var cellIndex: Int?
    var payment : PaymentMethodModel? {
        didSet{
            if let pay = payment {
                if pay.paypalEmailId != nil {
                    lblAccountType.text = "Paypal account"
                    lblPAypalIdorHolderNameHeading.text = "Paypal id"
                    lblEmailIdorAccountNumberHeading.text = "Paypal email id"
                    
                    lblPAypalIdorHolderName.text = pay.paypalEmailId
                    lblEmailIdorAccountNumber.text = pay.paypalEmailId
                    
                }else{
                    lblAccountType.text = "Bank account"
                    lblPAypalIdorHolderNameHeading.text = "Bank holder name"
                    lblEmailIdorAccountNumberHeading.text = "Bank account no."
                    
                    lblPAypalIdorHolderName.text = pay.accountHolderName
                    lblEmailIdorAccountNumber.text = pay.accountNumber
                    
                }
            }
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func deletePaymentMethodButtonClicked(_ sender: UIButton) {
        self.delegate?.deletePaymentMethodSelected(index: cellIndex)
    }
}
