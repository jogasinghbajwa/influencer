//
//  CampaignTableCell.swift
//  Influencer
//
//  Created by iTz_saGGu on 31/08/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class CampaignTableCell: AbstractTableCell {

    @IBOutlet var activeStatusView: UIView!
    @IBOutlet var inactiveStatusView: UIView!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var imageBaseView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layoutIfNeeded()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.imageBaseView.roundCorners(corners: .bottomLeft, radius: 50)
            self.iconImageView.layer.cornerRadius = self.iconImageView.bounds.size.height/2
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUI(campaign: Campaign) {
        setPreferences(name: campaign.campaignName, status: campaign.status.int == 1, category: campaign.category, price: campaign.price, imageURL: campaign.image)
    }
    
    private func setPreferences(name: String, status: Bool, category: String, price: String, imageURL: String) {
        activeStatusView.isHidden = !status
        inactiveStatusView.isHidden = status
        categoryLabel.text = category
        iconImageView.setImage(urlString: imageURL)
        priceLabel.text = "$" + price
        nameLabel.text = name
//        iconImageView.layer.cornerRadius = iconImageView.bounds.size.height/2
    }
    
}
