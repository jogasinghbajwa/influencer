
//
//  PaymentTableCell.swift
//  Influencer
//
//  Created by iTz_saGGu on 05/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class PaymentTableCell: AbstractTableCell {
    @IBOutlet weak var amountView: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
       
    }
}
