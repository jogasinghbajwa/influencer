//
//  MenuTableCell.swift
//  Influencer
//
//  Created by iTz_saGGu on 03/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class MenuTableCell: AbstractTableCell {

    @IBOutlet weak var menuTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
