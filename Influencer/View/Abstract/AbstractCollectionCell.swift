//
//  AbstractCollectionCell.swift
//
//  Created by Lakhwinder Singh on 31/03/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import UIKit
// MARK: Custom delegate for select/unselect cell

protocol CollectionCellDelegate {
    func updateCell()
    func updateModel(_ model: AnyObject, cell: AbstractCollectionCell)
}

class AbstractCollectionCell: UICollectionViewCell {
    
    var delegate: CollectionCellDelegate!
    
    class var reuseId: String {
        return String(describing: self)
    }
    

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        perform(#selector(self.initViews), with: self, afterDelay: 0.01)
    }
    
    @objc func initViews() {
    }
    
    func viewAppear() {
    }
    
    func updateBackGroundColor(_ color: UIColor) {
    }
    
    func updateWithModel(_ model: AnyObject) {
    }
    
    func updateModel(_ model: AnyObject) {
        delegate.updateModel(model, cell: self)
    }
    
    func updateCellSize(_ size: CGFloat) {
    }
    
    class func getHeightFromText(_ text: String) -> CGSize? {
        return nil// Default implementation does almost nothing
    }
    
}


