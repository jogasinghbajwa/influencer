//
//  AbstractTableCell.swift
//  Dropneed
//
//  Created by Lakhwinder Singh on 31/03/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import UIKit

protocol TableCellDelegate {
    func updateCell()
    func updateModel(_ model: AnyObject, cell: AbstractTableCell)
}

class AbstractTableCell: UITableViewCell, UITextFieldDelegate {

    var delegate: TableCellDelegate!
    
    class var reuseId: String {
        return String(describing: self)
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
    }
    
    
    
    func viewAppear() {
    }
    
    func viewDisappear() {
    }
    
    func updateWithModel(_ model: AnyObject) {
    }
    
    func updateCell() {
        layoutSubviews()
        delegate.updateCell()
    }
    
    func updateModel(_ model: AnyObject) {
        delegate.updateModel(model, cell: self)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


