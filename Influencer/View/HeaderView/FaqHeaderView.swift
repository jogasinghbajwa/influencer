//
//  FaqHeaderView.swift
//  Influencer
//
//  Created by iTz_saGGu on 03/09/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class FaqHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var headerButton: UIButton!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initViews() {
        let xibview = viewFromNibForClass()
        self.addSubview(xibview)
        xibview.addConstraintToFillSuperview()
        self.layoutIfNeeded()
    }
    
    func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

}
