//
//  UITextField.swift
//  ArtTime
//
//  Created by Lakhwinder Singh on 02/08/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import UIKit

extension UITextField {
    
    func addLeftImage(_ image: UIImage) {
        let margin: CGFloat = 10
        leftViewMode = .always
        var width: CGFloat = 30
        width += margin

        let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: bounds.size.height))
        let imageView = UIImageView(frame: CGRect(x: margin, y: 0, width: width-margin, height: bounds.size.height))
        imageView.contentMode = .center
        imageView.clipsToBounds = true
        imageView.image = image
        view.addSubview(imageView)
        leftView = view
    }
    
    //MARK: Email validation variable
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
    
    func addBorderWithColorAndBorderWidth(_ borderColor : UIColor,width : CGFloat) {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = width
    }
    
    func setPlaceHolderColor(_ placeHolderColor : UIColor, placeHolder : String) {
        self.attributedPlaceholder = NSAttributedString(string: placeHolder,
                                                        attributes: [NSAttributedString.Key.foregroundColor: placeHolderColor])
    }
    
    func setCornerRadius(value : Float)  {
        self.layer.cornerRadius = CGFloat(value)
        self.clipsToBounds = true
    }
    
    var containsNonWhitespace: Bool {
        let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ'")
        let input = self.text
        if (self.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (input?.rangeOfCharacter(from: set.inverted) != nil ) {
            return true
        } else {
            return false
        }
    }

    var containsNonWhitespaceAndNonDigit: Bool {
        let set = CharacterSet(charactersIn: "0123456789")
        let input = self.text
        if (self.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (input?.rangeOfCharacter(from: set.inverted) == nil ) {
            return true
        } else {
            return false
        }
    }
    
    
}


