//
//  UIButton.swift
//  ArtTime
//
//  Created by Lakhwinder Singh on 01/08/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import UIKit

extension UIButton {
    
    var image: UIImage {
        get {
            return image(for: .normal)!
        }
        set {
            setImage(newValue, for: .normal)
        }
    }
    
    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func setCornerRadius(value : Float)  {
        self.layer.cornerRadius = CGFloat(value)
        self.clipsToBounds = true
    }
}


