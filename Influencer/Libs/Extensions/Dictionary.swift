//
//  Dictionary.swift
//  Influencer
//
//  Created by Joga Singh on 03/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

extension NSDictionary {
    func toJSON() -> JSON? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data

            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            // here "decoded" is of type `Any`, decoded from JSON data
            return decoded as? JSON
            // you can now cast it with the right type
//            if let dictFromJSON = decoded as? [String:String] {
//                // use dictFromJSON
//            }
        } catch {
            return nil
        }
    }
}
