//
//  ImageView.swift
//  Influencer
//
//  Created by Joga Singh on 03/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func setImage(urlString: String?) {
        let placeholderImage = UIImage(named: "placeholderImage")
        guard let urlString = urlString, let url = URL(string: urlString) else {
            self.image = placeholderImage
            return
        }
        let processor = DownsamplingImageProcessor(size: self.bounds.size)
            |> RoundCornerImageProcessor(cornerRadius: 0)
        self.kf.indicatorType = .activity
        self.kf.setImage(
            with: url,
            placeholder: placeholderImage,
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
                break
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
}
