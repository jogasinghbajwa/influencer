 //
 //  AppDelegate.swift
 //  Influencer
 //
 //  Created by iTz_saGGu on 31/08/19.
 //  Copyright © 2019 iTz_saGGu. All rights reserved.
 //
 
 import UIKit
 import IQKeyboardManagerSwift
 import FBSDKCoreKit
 import FBSDKLoginKit
 import TwitterKit
 
 
 @UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var twitterCustomerKey = "lP58UzTip8RNqdbXnC3dOKEg0"
    var twitterConsumerSecretKey = "6CVHodIjbOfXB4uIWSqFDSC4bB462wA0jakRBOMcotgjK7upKu"
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        TWTRTwitter.sharedInstance().start(withConsumerKey: twitterCustomerKey, consumerSecret: twitterConsumerSecretKey)
        
        if UserDefaults.standard.bool(forKey: isUserLoggedIn){
            ActionShowHome.execute()
        }
//        UserDefaults.standard.set(true, forKey: isUserLoggedIn)
//        if let user = UserStore.shared.user {
//            
//        }
//        if let user = UserStore.shared.user.userId {
//            ActionShowHome.execute()
//        }
        
        return true
    }
    
    open func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
         let twitterHandler = TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        
        return twitterHandler
        
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
         let handled : Bool = ApplicationDelegate.shared.application(application , open: url, sourceApplication: sourceApplication, annotation: annotation)
        let twitterHandler = TWTRTwitter.sharedInstance().application(application, open: url, options: [:])
        if handled {
        return handled
        } else {
            return twitterHandler
        }
        
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
 }
 
