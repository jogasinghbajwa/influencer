//
//  UserStore.swift
//  CHOP
//
//  Created by Mac on 09/10/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import Foundation

class UserStore {
    
    //MARK: Shared instance
    static let shared = UserStore()
    
    //MARK: Variables
    var tempUser: User!
    
    var user: User! {
        get {
            return LocalStore.shared.user
        }
        set {
            LocalStore.shared.user = newValue
        }
    }
    
//    var userId: String! {
//        guard
//            let user = user,
//            let userId = "" //user.internalIdentifier else { return nil }
//        return userId
//    }
    
    var isLogin: Bool {
        get {
            return LocalStore.shared.isLogin
        }
        set {
            LocalStore.shared.isLogin = newValue
        }
    }
    
    
    
    //MARK: API Call for logout
    func logout() {
       
                self.logoutDone()
    
    }
    
    //MARK: Perform logout functionality
    func logoutDone() {
        self.isLogin = false
        self.user = nil
       
        ActionShowLogin.execute()
    }
 
   
    
}


