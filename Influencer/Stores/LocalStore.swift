//
//  LocalStore.swift
//  CHOP
//
//  Created by Mac on 09/10/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import Foundation

class LocalStore {
    
    static let shared = LocalStore()
    
    // MARK: User defaults keys
    private let firstLaunchKey = "firstLaunchKey"
    private let invitationSentKey = "invitationSentKey"
    private let invitedFriendsKey = "invitedFriendsKey"
    private let loginKey = "isLogin"
    private let userKey = "userKey"
    private let deviceTokenKey = "deviceTokenKey"
    private let categoryKey = "categoryKey"
    private let dmsNotiKey = "dmsNotiKey"
    private let alertNotiKey = "alertNotiKey"
    private let tutorialKey = "isSeeTutorial"
    
    //MARK: All country codes
   
    
    //MARK:- Local Settings Strings
    var Settings: [String] {
        return ["General and Privacy Settings", "Account Settings", "About Us", "Contact Us", "How to Use", "Terms and Conditions", "Send invite to CHOP to friends", "Logout"]
    }
    
    var PrivacySettings: [String] {
        return ["Tagging", "Recieve direct message from", "Get New Password", "Alerts"]
    }
    
    var Tagging: [String] {
        return ["Friends (Default)", "Everyone", "Nobody"]
    }
    
    var RecieveDirectMessage: String {
        return "Friends (Default)"
    }
    
  
    
    var dmsCount: String! {
        get {
            return UserDefaults.standard.value(forKey: dmsNotiKey) as? String ?? "0"
        }
        set {
            if newValue == nil {
                UserDefaults.standard.removeObject(forKey: dmsNotiKey)
            } else {
                UserDefaults.standard.set(newValue, forKey: dmsNotiKey)
            }
        }
    }
    
    var alertCount: String! {
        get {
            return UserDefaults.standard.value(forKey: alertNotiKey) as? String ?? "0"
        }
        set {
            if newValue == nil {
                UserDefaults.standard.removeObject(forKey: alertNotiKey)
            } else {
                UserDefaults.standard.set(newValue, forKey: alertNotiKey)
            }
        }
    }
    
    var totalCount: String {
        return (dmsCount == nil || alertCount == nil) ? "0" : "\(dmsCount.int + alertCount.int)"
    }
    
    var deviceToken: String {
        get {
            let deviceToken = UserDefaults.standard.value(forKey: deviceTokenKey)
            if deviceToken == nil {
                return "123"
            } else {
                let string = deviceToken as! String!
                return (string?.isEmpty)! ? "123" : string!
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: deviceTokenKey)
        }
    }
    
    var user: User! {
        get {
            let dict = UserDefaults.standard.value(forKey: userKey)
            if dict == nil {
                return nil
            } else {
                return User.init(fromDictionary: dict as! [String:Any])
            }
        }
        set {
            if newValue == nil {
                UserDefaults.standard.removeObject(forKey: userKey)
            } else {
                UserDefaults.standard.set(newValue.toDictionary(), forKey: userKey)
            }
        }
    }
    

    
    var isLogin: Bool {
        get {
            return UserDefaults.standard.bool(forKey: loginKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: loginKey)
        }
    }
    
    var isFirstLaunch: Bool {
        get {
            return UserDefaults.standard.bool(forKey: firstLaunchKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: firstLaunchKey)
        }
    }
    
    
}


