//
//  InfluencerApiStore2.swift
//  Influencer
//
//  Created by Manjodh Singh on 03/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


class  InfluencerApiStore2 {
    
    static let sharedInstance:InfluencerApiStore2  = InfluencerApiStore2()
    
    //MARK: Stored varibales
    var showProgress = true
    var showError = true
    var showMessage = false
    var retry = 0
    
    
    //MARK: Return network status
    class var isReachable: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    //MARK: Manager for api call
    static let manager: SessionManager = {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = 120
        return Alamofire.SessionManager(configuration: sessionConfiguration)
    }()
    
    //MARK: Auth
    func requestRegisterUser(param:[String:Any],completion:@escaping(_ success:Bool)->()) {
        self.showProgress = true
        self.showError = true
     
        requestBaseApiRequest(url: API_REGISTER_USER_POST,method: .post, param:param) { (dict) in
            if dict?["status"] as? Int != 0 {
                completion(true)
                self.showMessage(dict?["message"] as? String ?? "", true)
            } else {
                completion(false)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
}
    func getPaymentsListFromServer(param:[String:Any],completion:@escaping(_ success:Bool, _ compaignsList: [MyPaymentModel]?,_ currentMonthEarning:Int?,_ totalAmountEarned:Int?)->()) {
        self.showProgress = true
        self.showError = true
        
        requestBaseApiRequest(url: GET_MY_PAYMENTS_POST,method: .post, param:param) { (dict) in
            print(dict ?? "")
            
            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.myPayments,responceModel.current_month_earning,responceModel.total_amount_earned )
            } else {
                completion(false, nil,nil,nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    func addPaymentMethodFromServer(param:[String:Any],completion:@escaping(_ success:Bool, _ compaignsList: [MyPaymentModel]?,_ currentMonthEarning:Int?,_ totalAmountEarned:Int?)->()) {
        self.showProgress = true
        self.showError = true
        
        requestBaseApiRequest(url: ADD_PAYMENT_METHOD_POST,method: .post, param:param) { (dict) in
            print(dict ?? "")
            
            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.myPayments,responceModel.current_month_earning,responceModel.total_amount_earned )
            } else {
                completion(false, nil,nil,nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    func getFAQFromServer(param:[String:Any],completion:@escaping(_ success:Bool, _ compaignsList: [FAQModel]?)->()) {
        self.showProgress = true
        self.showError = true
        
        requestBaseApiRequest(url: GET_GET_FAQ_POST ,method: .post, param:param) { (dict) in
            print(dict ?? "")
            
            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.faq)
            } else {
                completion(false, nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    func getPaymentDetails(param:[String:Any],completion:@escaping(_ success:Bool, _ paymentArray: [PaymentMethodModel]?)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: GET_PAYMENT_METHODS_POST,method: .post, param:param) { (dict) in
            print(dict ?? "")
            
            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.payment_methods)
            } else {
                completion(false, nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    func getHelpFromServer(param:[String:Any],completion:@escaping(_ success:Bool, _ compaignsList: HelpModel?)->()) {
        self.showProgress = true
        self.showError = true
        
        requestBaseApiRequest(url: GET_Help_POST ,method: .post, param:param) { (dict) in
            print(dict ?? "")
            
            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.help)
            } else {
                completion(false, nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    
    func addSocialMediaAccount(param:[String:Any],completion:@escaping(_ success:Bool)->()) {
        print(param)
        self.showProgress = true
        self.showError = true
        let fbUrl = ADD_SOCIAL_MEDIA_ACCOUNT
        requestBaseApiRequest(url: fbUrl,method: .post, param:param) { (dict) in
            if dict?["status"] as? Int != 0 {
                completion(true)
            } else {
                completion(false)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }

    func requestBaseApiRequest(url:URLConvertible,method:HTTPMethod,param:Parameters? = nil,completion:@escaping(_:NSDictionary?)->() ) {
        if showProgress && !SVProgressHUD.isVisible() {
            SVProgressHUD.show()
        } else {
            showProgress = true
        }
        
        
        InfluencerApiStore.manager.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody).responseJSON { response in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success:
                
                self.getValidResult(response.result, url, param, method: method, completion: { (dict) in
                    completion(dict)
                })
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func getValidResult(_ result: Result<Any>, _ url: URLConvertible, _ parameters: Parameters? = nil, method: HTTPMethod, completion: @escaping (_ : NSDictionary?) -> Void) {
        self.getValidDict(result: result, completion: {(dict, error, retry) in
            
            if retry! {
                self.requestBaseApiRequest(url: url as! String, method: method, param: parameters, completion: completion)
                return
            }
            var dict = dict
            if dict == nil {
                dict = NSDictionary.init(dictionary:
                    [kBaseMessageKey: error?.localizedDescription ?? "Some error has been occured",
                     kBaseStatusKey: false])
            }
            //Get message from server
            let message = dict?[kBaseMessageKey] as! String
            let status = dict?[kBaseStatusKey] as! Bool
            if status == false && self.showError {
                self.showMessage(message, status)
            }//Get Error message from server
            if status == true && self.showMessage {
                self.showMessage(message, status)
            }//Get Success message from server
            completion (dict)
        })
    }

    private func getValidDict(result: Result<Any>, completion: @escaping (_ : NSDictionary?, _ : NSError?, _ : Bool?) -> Void) {
        var dict: NSDictionary!
        let errorNew = result.error as NSError?
        if let json = result.value {
            dict = json as? NSDictionary
        }
        
        
        if dict == nil && errorNew != nil && (errorNew?._code == NSURLErrorTimedOut || errorNew?.localizedDescription == "The network connection was lost.") {
            if retry >= 2 {
                UIAlertController.showAlert((errorNew?.localizedDescription)!, message: "", buttons: ["Cancel", "Retry"], completion: { (alert, index) in
                    if index == 0 {
                        completion (dict, errorNew, false)
                    } else {
                        completion (dict, errorNew, true)
                    }
                })
            } else {
                retry += 1
                DispatchQueue.dispatch_main_after(1.0, block: {
                    completion (dict, errorNew, true)
                })
            }
        } else {
            completion (dict, errorNew, false)
        }
    }

    //MARK: Show message from server
    func showMessage(_ message: String, _ status: Bool) {
        SVProgressHUD.show()
        if status {
            SVProgressHUD.showSuccess(withStatus: message)
        } else {
            SVProgressHUD.showError(withStatus: message)
        }
    }
    
}

//https://myprojectdemonstration.com/development/influencernew/api/authentication/add_social_media_account
//func addSocialMediaAccountAPI(param:[String:Any], @escaping(_ isAdded:Bool)->Void){
//    
//    
//}
