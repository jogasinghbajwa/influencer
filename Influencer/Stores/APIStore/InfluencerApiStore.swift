
//
//  InfulencerApiStore.swift
//  Influencer
//
//  Created by iTz_saGGu on 13/10/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class  InfluencerApiStore {
    static let sharedInstance:InfluencerApiStore  = InfluencerApiStore()
    
    //MARK: Stored varibales
    var showProgress = true
    var showError = true
    var showMessage = false
    var retry = 0
    
    
    //MARK: Return network status
    class var isReachable: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    //MARK: Manager for api call
    static let manager: SessionManager = {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = 120
        return Alamofire.SessionManager(configuration: sessionConfiguration)
    }()
    
    //MARK: Auth
    func requestRegisterUser(param:[String:Any],completion:@escaping(_ success:Bool)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: API_REGISTER_USER_POST,method: .post, param:param) { (dict) in
            if dict?["status"] as? Int != 0 {
                completion(true)
                self.showMessage(dict?["message"] as? String ?? "", true)
            } else {
                completion(false)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    func requestLoginUser(param:[String:Any],completion:@escaping(_ success:Bool)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: API_LOGIN_USER_POST,method: .post, param:param) { (dict) in
            if dict?["status"] as? Int != 0 {
                
                //                self.showMessage(dict?["message"] as? String ?? "", true)
                UserStore.shared.user = User.init(fromDictionary: dict?["user_info"] as? [String:Any] ?? [String:Any]())
                completion(true)
            } else {
                completion(false)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    //MARK: Campaign
    func getCampaignList(param:[String:Any],completion:@escaping(_ success:Bool, _ compaignsList: [Campaign]?)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: GET_USER_CAMPAIGN,method: .post, param:param) { (dict) in
            print(dict ?? "")

            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.compaigns)
            } else {
                completion(false, nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    func getCampaignDetail(param:[String:Any],completion:@escaping(_ success:Bool, _ campaignDetail: CampaignDetail?)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: GET_USER_CAMPAIGN_DETAIL,method: .post, param:param) { (dict) in
            print(dict ?? "")

            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.campaignDetail)
            } else {
                completion(false, nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    func requestUserProfileDetail(param:[String:Any],completion:@escaping(_ success:Bool,_ userDetail:ProfileModel?)->()) {
        self.showProgress = true
        self.showError = true
        let url = "https://myprojectdemonstration.com/development/influencernew/api/authentication/my_account"
          
        requestBaseApiRequest(url: MY_ACCOUNT_DETAIL_POST, method: .post, param:param) { (dict) in
            if dict?["status"] as? Int != 0 {
                let base = Response(fromDictionary: dict as! [String : Any] ?? [String:Any]() )
                //BaseCountry.init(fromDictionary: dict as? [String:Any] ?? [String:Any]())
                completion(true,base.userDetail)
            } else {
                completion(false,nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }

    
    func getAlertList(param:[String:Any],completion:@escaping(_ success:Bool, _ compaignsList: [UserNotification]?)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: GET_USER_NOTIFICATIONS, method: .post, param:param) { (dict) in
            print(dict ?? "")

            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.notifications)
            } else {
                completion(false, nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    func postComment(param:[String:Any],completion:@escaping(_ success:Bool)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: POST_COMMENT, method: .post, param:param) { (dict) in
            if dict?["status"] as? Int != 0 {
                completion(true)
            } else {
                completion(false)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
        }
    }
    
    func acceptOrRejectCampaign(param:[String:Any], isAccept: Bool, completion:@escaping(_ success:Bool)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: isAccept ? ACCEPT_CAMPAIGN : REJECT_CAMPAIGN, method: .post, param:param) { (dict) in
            if dict?["status"] as? Int != 0 {
                completion(true)
            } else {
                completion(false)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
        }
    }
    
    func enableOrDisableNotifications(param:[String:Any], completion:@escaping(_ success:Bool)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: ENABLE_DISABLE_NOTIFICATIONS, method: .post, param:param) { (dict) in
            if dict?["status"] as? Int != 0 {
                completion(true)
            } else {
                completion(false)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
        }
    }
    
    
    func getAboutUS(param:[String:Any], completion:@escaping(_ success:Bool,_ model: AboutUsModel?)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: GET_ABOUT_US, method: .post, param:param) { (dict) in
            print(dict ?? "")

            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.aboutUs)
            } else {
                completion(false, nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    
    //MARK: Country & states
    func requestGetAllCountries(param:[String:Any],completion:@escaping(_ success:Bool,_ _array :[Country])->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: API_ALL_COUNTRIES_GET,method: .post, param:param) { (dict) in
            if dict?["status"] as? Int != 0 {
                let base = BaseCountry.init(fromDictionary: dict as? [String:Any] ?? [String:Any]())
                completion(true,base.countries)
            } else {
                completion(false,[])
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    //MARK: Update profile
    
    func requestUpdateUserProfile(param:[String:Any],completion:@escaping(_ success:Bool,_ userDetail:ProfileModel?)->()) {
        requestMultipartAPI(UPDATE_PROFILE_POST, parameters: param, requestType: .post) { (dict) in
            
            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.userDetail)
            } else {
                completion(false, nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
            
        }
    }
    
    //MARK: Get Settings
    func getSocialMediaLinkStaus(param:[String:Any],completion:@escaping(_ success:Bool, _ campaignDetail: SocialMediaLinkStatus?)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: SOCIAL_MEDIA_ACCOUNT_STATUS,method: .post, param:param) { (dict) in
            print(dict ?? "")

            if dict?["status"] as? Int != 0 {
                let responceModel = Response(fromDictionary: dict as! [String : Any])
                completion(true, responceModel.socialMediaLinkStatus)
            } else {
                completion(false, nil)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
            
        }
    }
    
    func deletePaymentMethod(param:[String:Any], completion:@escaping(_ success:Bool)->()) {
        self.showProgress = true
        self.showError = true
        requestBaseApiRequest(url: DELETE_PAYMENT_METHODS, method: .post, param:param) { (dict) in
            if dict?["status"] as? Int != 0 {
                completion(true)
            } else {
                completion(false)
                self.showMessage(dict?["message"] as? String ?? "", false)
            }
        }
    }
}

extension InfluencerApiStore {
    
    private func requestBaseApiRequest(url:URLConvertible,method:HTTPMethod,param:Parameters? = nil,completion:@escaping(_:NSDictionary?)->() ) {
        if showProgress && !SVProgressHUD.isVisible() {
            SVProgressHUD.show()
        } else {
            showProgress = true
        }
        
        
        InfluencerApiStore.manager.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody).responseJSON { response in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success:
                
                self.getValidResult(response.result, url, param, method: method, completion: { (dict) in
                    completion(dict)
                })
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    //MARK: Get valid result
    private func getValidResult(_ result: Result<Any>, _ url: URLConvertible, _ parameters: Parameters? = nil, method: HTTPMethod, completion: @escaping (_ : NSDictionary?) -> Void) {
        self.getValidDict(result: result, completion: {(dict, error, retry) in
            
            if retry! {
                self.requestBaseApiRequest(url: url as! String, method: method, param: parameters, completion: completion)
                return
            }
            var dict = dict
            if dict == nil {
                dict = NSDictionary.init(dictionary:
                    [kBaseMessageKey: error?.localizedDescription ?? "Some error has been occured",
                     kBaseStatusKey: false])
            }
            //Get message from server
            let message = dict?[kBaseMessageKey] as! String
            let status = dict?[kBaseStatusKey] as! Bool
            if status == false && self.showError {
                self.showMessage(message, status)
            }//Get Error message from server
            if status == true && self.showMessage {
                self.showMessage(message, status)
            }//Get Success message from server
            completion (dict)
        })
    }
    
    //MARK: Get valid dictionary result
    private func getValidDict(result: Result<Any>, completion: @escaping (_ : NSDictionary?, _ : NSError?, _ : Bool?) -> Void) {
        var dict: NSDictionary!
        let errorNew = result.error as NSError?
        if let json = result.value {
            dict = json as? NSDictionary
        }
        
        
        if dict == nil && errorNew != nil && (errorNew?._code == NSURLErrorTimedOut || errorNew?.localizedDescription == "The network connection was lost.") {
            if retry >= 2 {
                UIAlertController.showAlert((errorNew?.localizedDescription)!, message: "", buttons: ["Cancel", "Retry"], completion: { (alert, index) in
                    if index == 0 {
                        completion (dict, errorNew, false)
                    } else {
                        completion (dict, errorNew, true)
                    }
                })
            } else {
                retry += 1
                DispatchQueue.dispatch_main_after(1.0, block: {
                    completion (dict, errorNew, true)
                })
            }
        } else {
            completion (dict, errorNew, false)
        }
    }
    
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    //MARK: Base request API
    private func requestMultipartAPI(_ url: URLConvertible, parameters: Parameters? = nil, requestType: HTTPMethod? = nil, completion: @escaping (_ : NSDictionary?) -> Void) {
        
        if showProgress && !SVProgressHUD.isVisible() {
            SVProgressHUD.show()
        } else {
            showProgress = true
        }
        InfluencerApiStore.manager.upload(multipartFormData: { (multipartFormData) in
            // For Image and other params
            for (key, value) in parameters! {
                if value is UIImage {
                    
                    (value as! UIImage).jpegData(compressionQuality: 0.3)
                    multipartFormData.append((value as! UIImage).jpegData(compressionQuality: 0.5)!, withName: "profile_image",fileName: "\(self.randomString(length: 5)).jpeg", mimeType: "file")
                    
                } else if value is String || value is Int {
                    multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                } else {
                    SVProgressHUD.showError(withStatus: "Parameters error")
                }
            }
        }, to: url, method: requestType ?? .post, headers: ["x-api-key": "CODEX@123","Content-Type": "application/json"],encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.response {
                    [weak self] response in
                    guard self != nil
                        else {
                            return
                    }
                    upload.responseJSON(completionHandler: { response in
                        SVProgressHUD.dismiss()
                        self?.getValidResult(response.result, url, parameters!, method: requestType ?? .get, completion: { (dict) in
                            completion(dict)
                        })
                    })
                }
            case .failure(_):
                completion (nil)
            }
        })
    }
    
    //MARK: Show message from server
    func showMessage(_ message: String, _ status: Bool) {
        SVProgressHUD.show()
        if status {
            SVProgressHUD.showSuccess(withStatus: message)
        } else {
            SVProgressHUD.showError(withStatus: message)
        }
    }
}
