


let BASE_URL:String = "https://myprojectdemonstration.com/development/influencernew/api/"
let IMAGE_BASE_URL = "https://myprojectdemonstration.com/development/influencernew/uploads/campaign/"
let AUTHENTICATION = BASE_URL + "authentication/"
let API_LOGIN_USER_POST = BASE_URL + "authentication/login"
let API_REGISTER_USER_POST = BASE_URL + "authentication/registration"
let API_ALL_COUNTRIES_GET = BASE_URL + "authentication/get_countries"
let API_ALL_STATES_GET = BASE_URL + "authentication/get_states"
let UPDATE_PROFILE_POST = BASE_URL + "authentication/update_profile"
let MY_ACCOUNT_DETAIL_POST = BASE_URL + "authentication/my_account"
let GET_MY_PAYMENTS_POST  = BASE_URL + "authentication/get_my_payments"
let GET_GET_FAQ_POST  = BASE_URL + "authentication/get_faq"
let GET_Help_POST  = BASE_URL + "authentication/get_help"
let GET_PAYMENT_METHODS_POST = BASE_URL + "authentication/get_payment_methods"
let ADD_PAYMENT_METHOD_POST = BASE_URL + "authentication/add_payment_method"
// Joga
let GET_USER_CAMPAIGN = AUTHENTICATION + "user_campaign"
let GET_USER_CAMPAIGN_DETAIL = AUTHENTICATION + "campaign_detail"
let GET_USER_NOTIFICATIONS = AUTHENTICATION + "get_notifications"
let POST_COMMENT = AUTHENTICATION + "post_comment"
let ADD_SOCIAL_MEDIA_ACCOUNT = AUTHENTICATION + "add_social_media_account"
let ACCEPT_CAMPAIGN = AUTHENTICATION + "accept_campaign"
let REJECT_CAMPAIGN = AUTHENTICATION + "reject_campaign"
let ENABLE_DISABLE_NOTIFICATIONS = AUTHENTICATION + "enable_disable_notifications"
let GET_ABOUT_US = AUTHENTICATION + "get_about_us"
let ADD_PAYMENT_METHOD = AUTHENTICATION + "add_payment_method"
let SOCIAL_MEDIA_ACCOUNT_STATUS = AUTHENTICATION + "social_media_account_status"
let DELETE_PAYMENT_METHODS = AUTHENTICATION + "delete_payment_methods"



