//
//  APIStore.swift
//  CHOP
//
//  Created by Mac on 12/10/17.
//  Copyright © 2017 lakh. All rights reserved.
//
import UIKit
import Alamofire

/*
 Base classs for api calls
 */
class APIStore: NSObject {
    
    //MARK: Stored varibales
    var showProgress = true
    var showError = true
    var showMessage = false
    var retry = 0
    
    static let sharedInstance = APIStore()
    
    //MARK: Return network status
    class var isReachable: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    //MARK: Return userId
    var userId: String {
        return ""//UserStore.shared.userId
    }
    
    //MARK: Manager for api call
    static let manager: SessionManager = {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = 120
        return Alamofire.SessionManager(configuration: sessionConfiguration)
    }()
    
    //MARK: Request API methods
    func requestBaseAPI(_ url: URLConvertible, parameters: Parameters? = nil, requestType: HTTPMethod? = nil, encoding: URLEncoding = .httpBody, completion: @escaping (_ : BaseAPIModel) -> Void) {
        requestAPI(url, parameters: parameters, requestType: requestType, completion: { (dict) in
            let baseModel = BaseAPIModel.init(object: dict!)
            completion (baseModel)
        })
    }
    
    //MARK: Request to get dictionary
    func requestDict(_ url: URLConvertible, parameters: Parameters? = nil, requestType: HTTPMethod? = nil, isUploadAPI: Bool? = false, encoding: URLEncoding = .default, completion: @escaping ( _ : NSDictionary?) -> Void) {
        
        if isUploadAPI! {
            requestMultipartAPI(url, parameters: parameters, requestType: requestType, completion: { (dict) in
                completion (dict)
            })
        } else {
            requestAPI(url, parameters: parameters, requestType: requestType, completion: { (dict) in
                completion (dict)
            })
        }
    }
    
    //MARK: Base request API
    private func requestAPI(_ url: URLConvertible, parameters: Parameters? = nil, requestType: HTTPMethod? = nil, encoding: URLEncoding = .httpBody, completion: @escaping (_ : NSDictionary?) -> Void) {
        if showProgress && !SVProgressHUD.isVisible() {
            SVProgressHUD.show()
        } else {
            showProgress = true
        }
        APIStore.manager.request(url, method: requestType ?? .post, parameters: parameters, encoding: encoding, headers: ["x-api-key": "CODEX@123","Content-Type": "application/json"]).responseJSON { response in
            if SVProgressHUD.isVisible() {
                SVProgressHUD.dismiss()
            }
            self.getValidResult(response.result, url, parameters, completion: { (dict) in
                completion(dict)
            })
        }
    }
    
    //MARK: Base request API
    private func requestMultipartAPI(_ url: URLConvertible, parameters: Parameters? = nil, requestType: HTTPMethod? = nil, completion: @escaping (_ : NSDictionary?) -> Void) {
        
        if showProgress && !SVProgressHUD.isVisible() {
            SVProgressHUD.show()
        } else {
            showProgress = true
        }
        APIStore.manager.upload(multipartFormData: { (multipartFormData) in
            // For Image and other params
            for (key, value) in parameters! {
                if value is UIImage {
                    
//                    if let imageData =  UIImage.jpegData(value as! UIImage) {
//                         multipartFormData.append(imageData.base64EncodedData(), withName: key)
//                    }
                
//                    if let imageData = UIImageJPEGRepresentation(value as! UIImage, 0.6) {
//
//                    }
                } else if value is String || value is Int {
                    multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                } else {
                    SVProgressHUD.showError(withStatus: "Parameters error")
                }
            }
        }, to: url, method: requestType ?? .post, headers: ["x-api-key": "CODEX@123","Content-Type": "application/json"],encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.response {
                    [weak self] response in
                    guard self != nil
                        else {
                            return
                    }
                    upload.responseJSON(completionHandler: { response in
                        SVProgressHUD.dismiss()
                        self?.getValidResult(response.result, url, parameters!, completion: { (dict) in
                            completion(dict)
                        })
                    })
                }
            case .failure(_):
                completion (nil)
            }
        })
    }
    
    //MARK: Get valid result
    private func getValidResult(_ result: Result<Any>, _ url: URLConvertible, _ parameters: Parameters? = nil, completion: @escaping (_ : NSDictionary?) -> Void) {
        self.getValidDict(result: result, completion: {(dict, error, retry) in
            if retry! {
                self.requestAPI(url, parameters: parameters, completion: completion)
                return
            }
            var dict = dict
            if dict == nil {
                dict = NSDictionary.init(dictionary:
                    [kBaseMessageKey: error?.localizedDescription ?? "Some error has been occured",
                     kBaseStatusKey: false])
            }
            //Get message from server
            let message = dict?[kBaseMessageKey] as! String
            let status = dict?[kBaseStatusKey] as! Bool
            if status == false && self.showError {
                self.showMessage(message, status)
            }//Get Error message from server
            if status == true && self.showMessage {
                self.showMessage(message, status)
            }//Get Success message from server
            completion (dict)
        })
    }
    
    //MARK: Get valid dictionary result
    private func getValidDict(result: Result<Any>, completion: @escaping (_ : NSDictionary?, _ : NSError?, _ : Bool?) -> Void) {
        var dict: NSDictionary!
        let errorNew = result.error as NSError?
        if let json = result.value {
            dict = json as! NSDictionary
        }
        if dict == nil && errorNew != nil && (errorNew?._code == NSURLErrorTimedOut || errorNew?.localizedDescription == "The network connection was lost.") {
            if retry >= 2 {
                UIAlertController.showAlert((errorNew?.localizedDescription)!, message: "", buttons: ["Cancel", "Retry"], completion: { (alert, index) in
                    if index == 0 {
                        completion (dict, errorNew, false)
                    } else {
                        completion (dict, errorNew, true)
                    }
                })
            } else {
                retry += 1
                DispatchQueue.dispatch_main_after(1.0, block: {
                    completion (dict, errorNew, true)
                })
            }
        } else {
            completion (dict, errorNew, false)
        }
    }
    
    //MARK: Handle search query
    func handleSearchQuery(_ url: String!) {
        APIStore.manager.session.getAllTasks { (tasks) -> Void in
            for task in tasks {
                if (task.originalRequest?.url?.absoluteString.contains(url))! {
                    task.cancel()
                }
            }
        }
    }
    
    //MARK: Show message from server
    func showMessage(_ message: String, _ status: Bool) {
        SVProgressHUD.show()
        if status {
            SVProgressHUD.showSuccess(withStatus: message)
        } else {
            SVProgressHUD.showError(withStatus: message)
        }
    }
    
}


