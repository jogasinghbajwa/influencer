//
//  Constants.swift
//  Influencer
//
//  Created by iTz_saGGu on 31/08/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import Foundation
import UIKit

let APP_NAME = "Influencer"

//MARK: STORYBOADR IDSw

let LOGIN_SB_ID:String = "LoginController"
let HOME_NAV_SB_ID:String = "HomeTabBar"
let LOGIN_NAV_SB_ID:String = "loginNav"



//MARK: COLORS
let NAV_GRADINET_BLUE:UIColor = UIColor(red:0.00, green:0.45, blue:1.00, alpha:1.0)

let NAV_GRADINET_RED:UIColor = UIColor(red:0.93, green:0.10, blue:0.35, alpha:1.0)
let isUserLoggedIn = "isUserLoggedIn"

enum ParameterNames: String {
    case user_id = "user_id"
    case order = "order"
    case campaign_id = "campaign_id"
    case category_id = "category_id"
    case comment = "comment"
    case sort = "sort"
    case is_notifications_enabled = "is_notifications_enabled"
    case request_type = "request_type"
    case payment_method_id = "payment_method_id"
}



func unwrapString(string:String?)->String {
    if let newString = string {
        return newString
    }else{
        return ""
    }
}

func unwrapInt(int:Int?)->Int {
    if let newInt = int {
        return newInt
    }else{
        return 0
    }
}
