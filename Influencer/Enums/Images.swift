//
//  Images.swift
//  Influencer
//
//  Created by iTz_saGGu on 31/08/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import Foundation
import UIKit

struct Images {
    static var Login: UIImage  {
        get {
            switch UIDevice().screenType {
            case .iPhones_5_5s_5c_SE:
                return UIImage.init(named: "iPhone5_bg.png")!
            case .iPhones_6_6s_7_8:
                return UIImage.init(named: "iPhone6_bg.png")!
            case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                return UIImage.init(named: "iPhone7P_bg")!
            case .iPhone_XSMax:
                return UIImage.init(named: "iPhoneX_bg.png")!
            case .iPhones_X_XS,.iPhone_XR:
                return UIImage.init(named: "iPhone6_bg.png")!
            case .unknown:
                return UIImage.init(named: "iPhoneX_bg.png")!
            default:
                return UIImage.init(named: "iPhoneX_bg.png")!
            }
        }
       
        
//        return UIColor(red: 1, green: 0, blue: 0, alpha: 1)
        
        
    }
    
}
