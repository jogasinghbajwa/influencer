//
//  Notification.swift
//  Influencer
//
//  Created by Joga Singh on 03/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//


import Foundation


class UserNotification : NSObject, NSCoding{

    var date : String!
    var id : String!
    var notificationDescription : String!
    var notificationImage : String!
    var notificationName : String!
    var notificationType : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        date = dictionary["date"] as? String
        id = dictionary["id"] as? String
        notificationDescription = dictionary["notification_description"] as? String
        notificationImage = dictionary["notification_image"] as? String
        notificationName = dictionary["notification_name"] as? String
        notificationType = dictionary["notification_type"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if date != nil{
            dictionary["date"] = date
        }
        if id != nil{
            dictionary["id"] = id
        }
        if notificationDescription != nil{
            dictionary["notification_description"] = notificationDescription
        }
        if notificationImage != nil{
            dictionary["notification_image"] = notificationImage
        }
        if notificationName != nil{
            dictionary["notification_name"] = notificationName
        }
        if notificationType != nil{
            dictionary["notification_type"] = notificationType
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         date = aDecoder.decodeObject(forKey: "date") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         notificationDescription = aDecoder.decodeObject(forKey: "notification_description") as? String
         notificationImage = aDecoder.decodeObject(forKey: "notification_image") as? String
         notificationName = aDecoder.decodeObject(forKey: "notification_name") as? String
         notificationType = aDecoder.decodeObject(forKey: "notification_type") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if notificationDescription != nil{
            aCoder.encode(notificationDescription, forKey: "notification_description")
        }
        if notificationImage != nil{
            aCoder.encode(notificationImage, forKey: "notification_image")
        }
        if notificationName != nil{
            aCoder.encode(notificationName, forKey: "notification_name")
        }
        if notificationType != nil{
            aCoder.encode(notificationType, forKey: "notification_type")
        }

    }

}
