//
//	Country.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Country : NSObject, NSCoding{

	var countryId : String!
	var countryName : String!
	var sortname : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		countryId = dictionary["country_id"] as? String
		countryName = dictionary["country_name"] as? String
		sortname = dictionary["sortname"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if countryName != nil{
			dictionary["country_name"] = countryName
		}
		if sortname != nil{
			dictionary["sortname"] = sortname
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         countryName = aDecoder.decodeObject(forKey: "country_name") as? String
         sortname = aDecoder.decodeObject(forKey: "sortname") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if countryName != nil{
			aCoder.encode(countryName, forKey: "country_name")
		}
		if sortname != nil{
			aCoder.encode(sortname, forKey: "sortname")
		}

	}

}