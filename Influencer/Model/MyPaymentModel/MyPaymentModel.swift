//
//  MyPaymentModel.swift
//  Influencer
//
//  Created by Manjodh Singh on 04/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import Foundation


class MyPaymentModel : NSObject, NSCoding{
    
    var amountEarned : String!
    var campaignId : String!
    var campaignImage : String!
    var campaignName : String!
    var paymentDate : AnyObject!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        amountEarned = dictionary["amount_earned"] as? String
        campaignId = dictionary["campaign_id"] as? String
        campaignImage = dictionary["campaign_image"] as? String
        campaignName = dictionary["campaign_name"] as? String
        paymentDate = dictionary["payment_date"] as? AnyObject
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amountEarned != nil{
            dictionary["amount_earned"] = amountEarned
        }
        if campaignId != nil{
            dictionary["campaign_id"] = campaignId
        }
        if campaignImage != nil{
            dictionary["campaign_image"] = campaignImage
        }
        if campaignName != nil{
            dictionary["campaign_name"] = campaignName
        }
        if paymentDate != nil{
            dictionary["payment_date"] = paymentDate
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        amountEarned = aDecoder.decodeObject(forKey: "amount_earned") as? String
        campaignId = aDecoder.decodeObject(forKey: "campaign_id") as? String
        campaignImage = aDecoder.decodeObject(forKey: "campaign_image") as? String
        campaignName = aDecoder.decodeObject(forKey: "campaign_name") as? String
        paymentDate = aDecoder.decodeObject(forKey: "payment_date") as? AnyObject
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if amountEarned != nil{
            aCoder.encode(amountEarned, forKey: "amount_earned")
        }
        if campaignId != nil{
            aCoder.encode(campaignId, forKey: "campaign_id")
        }
        if campaignImage != nil{
            aCoder.encode(campaignImage, forKey: "campaign_image")
        }
        if campaignName != nil{
            aCoder.encode(campaignName, forKey: "campaign_name")
        }
        if paymentDate != nil{
            aCoder.encode(paymentDate, forKey: "payment_date")
        }
        
    }
    
}
