//
//  Action.swift
//  CHOP
//
//  Created by Lakhwinder Singh on 09/10/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import UIKit

class ActionShow {
    
    
}

/*
 ActionShowHome to set Home feed as root controller
 */
class ActionShowHome {
    
    class func execute() {
//        UIApplication.appWindow.rootViewController = UIStoryboard.main.instantiateViewController(withIdentifier: HOME_NAV_SB_ID)
        
        let tabbarControl = UIStoryboard.main.instantiateViewController(withIdentifier: HOME_NAV_SB_ID) as! AbstractTabbarController
        let rightMenuControl = UIStoryboard.main.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        let darwerControl = MMDrawerController.init(center: tabbarControl, rightDrawerViewController: rightMenuControl)
        darwerControl?.showsShadow = false
        darwerControl?.shouldStretchDrawer = false
        
        darwerControl?.maximumLeftDrawerWidth = UIScreen.mainBounds.width * 0.3
        
        darwerControl?.openDrawerGestureModeMask = .panningCenterView
        darwerControl?.closeDrawerGestureModeMask = .tapCenterView
    
        UIApplication.appWindow.rootViewController = darwerControl
        
    }
}

/////////////////////////////////////////////////////////////////////

/*
 ActionShowLogin to set Login as root controller
 */
class ActionShowLogin {
    
    class func execute() {
        UIApplication.appWindow.rootViewController = UIStoryboard.main.instantiateViewController(withIdentifier: LOGIN_SB_ID)
    }
    
}


