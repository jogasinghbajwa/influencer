//
//  HelpModel.swift
//  Influencer
//
//  Created by Manjodh Singh on 05/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import Foundation

class HelpModel : NSObject, NSCoding{
    
    var address : String!
    var email : String!
    var id : String!
    var latitude : String!
    var longitude : String!
    var phone : String!
    
    
   
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String
        email = dictionary["email"] as? String
        id = dictionary["id"] as? String
        latitude = dictionary["latitude"] as? String
        longitude = dictionary["longitude"] as? String
        phone = dictionary["phone"] as? String
    }
    
   
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if email != nil{
            dictionary["email"] = email
        }
        if id != nil{
            dictionary["id"] = id
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if latitude != nil{
            aCoder.encode(latitude, forKey: "latitude")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        if phone != nil{
            aCoder.encode(phone, forKey: "phone")
        }
        
    }
    
}
