//
//  FAQModel.swift
//  Influencer
//
//  Created by Manjodh Singh on 04/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import Foundation


class FAQModel : NSObject, NSCoding{
    
    var answer : String!
    var id : String!
    var question : String!
    var isSelected : Bool = false
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        answer = dictionary["answer"] as? String
        id = dictionary["id"] as? String
        question = dictionary["question"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if answer != nil{
            dictionary["answer"] = answer
        }
        if id != nil{
            dictionary["id"] = id
        }
        if question != nil{
            dictionary["question"] = question
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        answer = aDecoder.decodeObject(forKey: "answer") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        question = aDecoder.decodeObject(forKey: "question") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if answer != nil{
            aCoder.encode(answer, forKey: "answer")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if question != nil{
            aCoder.encode(question, forKey: "question")
        }
        
    }
    
}
