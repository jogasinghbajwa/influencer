//
//  BaseAPIModel.swift
//  CHOP
//
//  Created by Mac on 12/10/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import UIKit

// MARK: Common Keys
let kBaseMessageKey: String = "message"
let kBaseStatusKey: String = "status"
let kBaseDataKey: String = "data"
let kBaseTotalPagesKey: String = "total_pages"

public class BaseAPIModel {
    
    // MARK: Properties
    public var status: Bool = false
    public var data: JSON?
    public var message: String?
    public var totalPages: Int?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    public convenience init(object: Any!) {
        self.init(json: JSON(object ?? ""))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON!) {
        status = json[kBaseStatusKey].boolValue
        data = json[kBaseDataKey]
        message = json[kBaseMessageKey].string
        totalPages = json[kBaseTotalPagesKey].int
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public var dictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[kBaseStatusKey] = status
        dictionary[kBaseDataKey] = data
        dictionary[kBaseTotalPagesKey] = totalPages
        if let value = message { dictionary[kBaseMessageKey] = value }
        return dictionary
    }
    
}


