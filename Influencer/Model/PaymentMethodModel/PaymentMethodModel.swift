//
//  PaymentMethodModel.swift
//  Influencer
//
//  Created by Manjodh Singh on 05/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import Foundation
import UIKit


class PaymentMethodModel : NSObject, NSCoding{
    
    var accountHolderName : String!
    var accountNumber : String!
    var bankName : String!
    var id : String!
    var paymentName : String!
    var paymentType : String!
    var paypalEmailId : String!
    var title : String!
    var userId : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        accountHolderName = dictionary["account_holder_name"] as? String
        accountNumber = dictionary["account_number"] as? String
        bankName = dictionary["bank_name"] as? String
        id = dictionary["id"] as? String
        paymentName = dictionary["payment_name"] as? String
        paymentType = dictionary["payment_type"] as? String
        paypalEmailId = dictionary["paypal_email_id"] as? String
        title = dictionary["title"] as? String
        userId = dictionary["user_id"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if accountHolderName != nil{
            dictionary["account_holder_name"] = accountHolderName
        }
        if accountNumber != nil{
            dictionary["account_number"] = accountNumber
        }
        if bankName != nil{
            dictionary["bank_name"] = bankName
        }
        if id != nil{
            dictionary["id"] = id
        }
        if paymentName != nil{
            dictionary["payment_name"] = paymentName
        }
        if paymentType != nil{
            dictionary["payment_type"] = paymentType
        }
        if paypalEmailId != nil{
            dictionary["paypal_email_id"] = paypalEmailId
        }
        if title != nil{
            dictionary["title"] = title
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        accountHolderName = aDecoder.decodeObject(forKey: "account_holder_name") as? String
        accountNumber = aDecoder.decodeObject(forKey: "account_number") as? String
        bankName = aDecoder.decodeObject(forKey: "bank_name") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        paymentName = aDecoder.decodeObject(forKey: "payment_name") as? String
        paymentType = aDecoder.decodeObject(forKey: "payment_type") as? String
        paypalEmailId = aDecoder.decodeObject(forKey: "paypal_email_id") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if accountHolderName != nil{
            aCoder.encode(accountHolderName, forKey: "account_holder_name")
        }
        if accountNumber != nil{
            aCoder.encode(accountNumber, forKey: "account_number")
        }
        if bankName != nil{
            aCoder.encode(bankName, forKey: "bank_name")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if paymentName != nil{
            aCoder.encode(paymentName, forKey: "payment_name")
        }
        if paymentType != nil{
            aCoder.encode(paymentType, forKey: "payment_type")
        }
        if paypalEmailId != nil{
            aCoder.encode(paypalEmailId, forKey: "paypal_email_id")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
        
    }
    
}
