//
//  Response.swift
//  Influencer
//
//  Created by Joga Singh on 03/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import Foundation
//import SwiftyJSON


class Response : NSObject, NSCoding{

    var message : String!
    var status : Bool!
    
    var compaigns : [Campaign]!
    var campaignDetail : CampaignDetail!
    var userDetail : ProfileModel!
    var notifications : [UserNotification]!
    var totalRecords : Int!
    var myPayments : [MyPaymentModel]!
    var faq : [FAQModel]!
    var total_amount_earned : Int!
    var current_month_earning : Int!
    var help : HelpModel!
    var aboutUs : AboutUsModel!
    var payment_methods : [PaymentMethodModel]!
    var socialMediaLinkStatus : SocialMediaLinkStatus!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        compaigns = [Campaign]()
        if let compaignsArray = dictionary["compaigns"] as? [[String:Any]]{
            for dic in compaignsArray{
                let value = Campaign(fromDictionary: dic)
                compaigns.append(value)
            }
        }
        if let dataData = dictionary["data"] as? [String:Any]{
            socialMediaLinkStatus = SocialMediaLinkStatus(fromDictionary: dataData)
        }
        myPayments = [MyPaymentModel]()
        if let paymentsArray = dictionary["payments"] as? [[String:Any]]{
            for dic in paymentsArray{
                let value = MyPaymentModel(fromDictionary: dic)
                myPayments.append(value)
            }
        }
        
        payment_methods = [PaymentMethodModel]()
        if let paymentsArray = dictionary["payment_methods"] as? [[String:Any]]{
            for dic in paymentsArray{
                let value = PaymentMethodModel(fromDictionary: dic)
                payment_methods.append(value)
            }
        }
        
        faq = [FAQModel]()
        if let paymentsArray = dictionary["faq"] as? [[String:Any]]{
            for dic in paymentsArray{
                let value = FAQModel(fromDictionary: dic)
                faq.append(value)
            }
        }
        
        notifications = [UserNotification]()
        if let notificationsArray = dictionary["notifications"] as? [[String:Any]]{
            for dic in notificationsArray{
                let value = UserNotification(fromDictionary: dic)
                notifications.append(value)
            }
        }
        
        if let campaignDetailData = dictionary["campaign_detail"] as? [String:Any]{
            campaignDetail = CampaignDetail(fromDictionary: campaignDetailData)
        }
        
//        if let paymentArray = dictionary["payment_methods"] as? [[String:Any]]{
//            for dic in paymentArray{
//                let value = PaymentMethodModel(fromDictionary: dic)
//                payment_methods.append(value)
//            }
//        }
        
        if let userDetailData = dictionary["user_info"] as? [String:Any]{
            userDetail = ProfileModel(fromDictionary: userDetailData)
            
        }
        
        if let HelpData = dictionary["help"] as? [String:Any]{
            help = HelpModel(fromDictionary: HelpData)
            
        }
        
        if let aboutUsData = dictionary["about_us"] as? [String:Any]{
            aboutUs = AboutUsModel(fromDictionary: aboutUsData)
        }
        
        totalRecords = dictionary["total_records"] as? Int
        total_amount_earned = dictionary["total_amount_earned"] as? Int
        current_month_earning = dictionary["current_month_earning"] as? Int
        
        
        message = dictionary["message"] as? String
        status = dictionary["status"] as? Bool
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if compaigns != nil{
            var dictionaryElements = [[String:Any]]()
            for compaignsElement in compaigns {
                dictionaryElements.append(compaignsElement.toDictionary())
            }
            dictionary["compaigns"] = dictionaryElements
        }
        
        if payment_methods != nil{
            var dictionaryElements = [[String:Any]]()
            for compaignsElement in payment_methods {
                dictionaryElements.append(compaignsElement.toDictionary())
            }
            dictionary["payment_methods"] = dictionaryElements
        }
        
        if myPayments != nil{
            var dictionaryElements = [[String:Any]]()
            for compaignsElement in myPayments {
                dictionaryElements.append(compaignsElement.toDictionary())
            }
            dictionary["payments"] = dictionaryElements
        }
        if socialMediaLinkStatus != nil{
            dictionary["data"] = socialMediaLinkStatus.toDictionary()
        }
        if faq != nil{
            var dictionaryElements = [[String:Any]]()
            for compaignsElement in faq {
                dictionaryElements.append(compaignsElement.toDictionary())
            }
            dictionary["faq"] = dictionaryElements
        }
        
        if campaignDetail != nil{
            dictionary["campaign_detail"] = campaignDetail.toDictionary()
        }
        
        if help != nil{
            dictionary["help"] = help.toDictionary()
        }
        
        if aboutUs != nil{
            dictionary["about_us"] = aboutUs.toDictionary()
        }
        
        if notifications != nil{
            var dictionaryElements = [[String:Any]]()
            for notificationsElement in notifications {
                dictionaryElements.append(notificationsElement.toDictionary())
            }
            dictionary["notifications"] = dictionaryElements
        }
        if message != nil{
            dictionary["message"] = message
        }
        if help != nil{
            dictionary["help"] = help
        }
        if status != nil{
            dictionary["status"] = status
        }
        if totalRecords != nil{
            dictionary["total_records"] = totalRecords
        }
        if total_amount_earned != nil{
            dictionary["total_amount_earned"] = total_amount_earned
        }
        if current_month_earning != nil{
            dictionary["current_month_earning"] = current_month_earning
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        
        socialMediaLinkStatus = aDecoder.decodeObject(forKey: "data") as? SocialMediaLinkStatus
        aboutUs = aDecoder.decodeObject(forKey: "about_us") as? AboutUsModel
        help = aDecoder.decodeObject(forKey :"help") as? HelpModel
        faq = aDecoder.decodeObject(forKey :"payments") as? [FAQModel]
        payment_methods = aDecoder.decodeObject(forKey :"payment_methods") as? [PaymentMethodModel]
        myPayments = aDecoder.decodeObject(forKey :"payments") as? [MyPaymentModel]
        compaigns = aDecoder.decodeObject(forKey :"compaigns") as? [Campaign]
        campaignDetail = aDecoder.decodeObject(forKey: "campaign_detail") as? CampaignDetail
        message = aDecoder.decodeObject(forKey: "message") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Bool
        notifications = aDecoder.decodeObject(forKey :"notifications") as? [UserNotification]
        totalRecords = aDecoder.decodeObject(forKey: "total_records") as? Int
        total_amount_earned = aDecoder.decodeObject(forKey: "total_amount_earned") as? Int
        current_month_earning = aDecoder.decodeObject(forKey: "current_month_earning") as? Int
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if socialMediaLinkStatus != nil{
            aCoder.encode(socialMediaLinkStatus, forKey: "data")
        }
        if aboutUs != nil{
            aCoder.encode(aboutUs, forKey: "about_us")
        }
        if compaigns != nil{
            aCoder.encode(compaigns, forKey: "compaigns")
        }
        if payment_methods != nil{
            aCoder.encode(payment_methods, forKey: "payment_methods")
        }
        if faq != nil{
            aCoder.encode(faq, forKey: "faq")
        }
        if help != nil{
            aCoder.encode(help, forKey: "help")
        }
        if myPayments != nil{
            aCoder.encode(myPayments, forKey: "payments")
        }
        if campaignDetail != nil{
            aCoder.encode(campaignDetail, forKey: "campaign_detail")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if notifications != nil{
            aCoder.encode(notifications, forKey: "notifications")
        }
        if totalRecords != nil{
            aCoder.encode(totalRecords, forKey: "total_records")
        }
        if total_amount_earned != nil{
            aCoder.encode(total_amount_earned, forKey: "total_amount_earned")
        }
        if current_month_earning != nil{
            aCoder.encode(current_month_earning, forKey: "current_month_earning")
        }

    }

}
