//
//	BaseCountry.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class BaseCountry : NSObject, NSCoding{

	var countries : [Country]!
	var message : String!
	var status : Bool!
	var totalRecords : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		countries = [Country]()
		if let countriesArray = dictionary["countries"] as? [[String:Any]]{
			for dic in countriesArray{
				let value = Country(fromDictionary: dic)
				countries.append(value)
			}
		}
		message = dictionary["message"] as? String
		status = dictionary["status"] as? Bool
		totalRecords = dictionary["total_records"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if countries != nil{
			var dictionaryElements = [[String:Any]]()
			for countriesElement in countries {
				dictionaryElements.append(countriesElement.toDictionary())
			}
			dictionary["countries"] = dictionaryElements
		}
		if message != nil{
			dictionary["message"] = message
		}
		if status != nil{
			dictionary["status"] = status
		}
		if totalRecords != nil{
			dictionary["total_records"] = totalRecords
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         countries = aDecoder.decodeObject(forKey :"countries") as? [Country]
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Bool
         totalRecords = aDecoder.decodeObject(forKey: "total_records") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if countries != nil{
			aCoder.encode(countries, forKey: "countries")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if totalRecords != nil{
			aCoder.encode(totalRecords, forKey: "total_records")
		}

	}

}