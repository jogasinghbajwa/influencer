//
//  SocialMediaLinkStatus.swift
//  Influencer
//
//  Created by Joga Singh on 06/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class SocialMediaLinkStatus : NSObject, NSCoding{

    var fbStatus : Int!
    var instagramStatus : Int!
    var snapchatStatus : Int!
    var twitterStatus : Int!
    var youtubeStatus : Int!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        fbStatus = dictionary["fb_status"] as? Int
        instagramStatus = dictionary["instagram_Status"] as? Int
        snapchatStatus = dictionary["snapchat_status"] as? Int
        twitterStatus = dictionary["twitter_status"] as? Int
        youtubeStatus = dictionary["youtube_status"] as? Int
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if fbStatus != nil{
            dictionary["fb_status"] = fbStatus
        }
        if instagramStatus != nil{
            dictionary["instagram_Status"] = instagramStatus
        }
        if snapchatStatus != nil{
            dictionary["snapchat_status"] = snapchatStatus
        }
        if twitterStatus != nil{
            dictionary["twitter_status"] = twitterStatus
        }
        if youtubeStatus != nil{
            dictionary["youtube_status"] = youtubeStatus
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         fbStatus = aDecoder.decodeObject(forKey: "fb_status") as? Int
         instagramStatus = aDecoder.decodeObject(forKey: "instagram_Status") as? Int
         snapchatStatus = aDecoder.decodeObject(forKey: "snapchat_status") as? Int
         twitterStatus = aDecoder.decodeObject(forKey: "twitter_status") as? Int
         youtubeStatus = aDecoder.decodeObject(forKey: "youtube_status") as? Int

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if fbStatus != nil{
            aCoder.encode(fbStatus, forKey: "fb_status")
        }
        if instagramStatus != nil{
            aCoder.encode(instagramStatus, forKey: "instagram_Status")
        }
        if snapchatStatus != nil{
            aCoder.encode(snapchatStatus, forKey: "snapchat_status")
        }
        if twitterStatus != nil{
            aCoder.encode(twitterStatus, forKey: "twitter_status")
        }
        if youtubeStatus != nil{
            aCoder.encode(youtubeStatus, forKey: "youtube_status")
        }

    }

}
