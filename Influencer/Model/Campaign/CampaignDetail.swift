//
//  CampaignDetail.swift
//  Influencer
//
//  Created by Joga Singh on 03/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit

class CampaignDetail : NSObject, NSCoding{

    var campaignId : Int!
    var campaignName : String!
    var campaignType : Int!
    var campaignTypeName : String!
    var category : String!
    var categoryId : Int!
    var dealCode : String!
    var descriptionField : String!
    var endDate : String!
    var image : String!
    var isCampaignAccepted : String!
    var postType : String!
    var price : String!
    var startDate : String!
    var status : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        campaignId = dictionary["campaign_id"] as? Int
        campaignName = dictionary["campaign_name"] as? String
        campaignType = dictionary["campaign_type"] as? Int
        campaignTypeName = dictionary["campaign_type_name"] as? String
        category = dictionary["category"] as? String
        categoryId = dictionary["category_id"] as? Int
        dealCode = dictionary["deal_code"] as? String
        descriptionField = dictionary["description"] as? String
        endDate = dictionary["end_date"] as? String
        image = dictionary["image"] as? String
        isCampaignAccepted = dictionary["is_campaign_accepted"] as? String
        postType = dictionary["post_type"] as? String
        price = dictionary["price"] as? String
        startDate = dictionary["start_date"] as? String
        status = dictionary["status"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if campaignId != nil{
            dictionary["campaign_id"] = campaignId
        }
        if campaignName != nil{
            dictionary["campaign_name"] = campaignName
        }
        if campaignType != nil{
            dictionary["campaign_type"] = campaignType
        }
        if campaignTypeName != nil{
            dictionary["campaign_type_name"] = campaignTypeName
        }
        if category != nil{
            dictionary["category"] = category
        }
        if categoryId != nil{
            dictionary["category_id"] = categoryId
        }
        if dealCode != nil{
            dictionary["deal_code"] = dealCode
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if endDate != nil{
            dictionary["end_date"] = endDate
        }
        if image != nil{
            dictionary["image"] = image
        }
        if isCampaignAccepted != nil{
            dictionary["is_campaign_accepted"] = isCampaignAccepted
        }
        if postType != nil{
            dictionary["post_type"] = postType
        }
        if price != nil{
            dictionary["price"] = price
        }
        if startDate != nil{
            dictionary["start_date"] = startDate
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         campaignId = aDecoder.decodeObject(forKey: "campaign_id") as? Int
         campaignName = aDecoder.decodeObject(forKey: "campaign_name") as? String
         campaignType = aDecoder.decodeObject(forKey: "campaign_type") as? Int
         campaignTypeName = aDecoder.decodeObject(forKey: "campaign_type_name") as? String
         category = aDecoder.decodeObject(forKey: "category") as? String
         categoryId = aDecoder.decodeObject(forKey: "category_id") as? Int
         dealCode = aDecoder.decodeObject(forKey: "deal_code") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         endDate = aDecoder.decodeObject(forKey: "end_date") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         isCampaignAccepted = aDecoder.decodeObject(forKey: "is_campaign_accepted") as? String
         postType = aDecoder.decodeObject(forKey: "post_type") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         startDate = aDecoder.decodeObject(forKey: "start_date") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if campaignId != nil{
            aCoder.encode(campaignId, forKey: "campaign_id")
        }
        if campaignName != nil{
            aCoder.encode(campaignName, forKey: "campaign_name")
        }
        if campaignType != nil{
            aCoder.encode(campaignType, forKey: "campaign_type")
        }
        if campaignTypeName != nil{
            aCoder.encode(campaignTypeName, forKey: "campaign_type_name")
        }
        if category != nil{
            aCoder.encode(category, forKey: "category")
        }
        if categoryId != nil{
            aCoder.encode(categoryId, forKey: "category_id")
        }
        if dealCode != nil{
            aCoder.encode(dealCode, forKey: "deal_code")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if endDate != nil{
            aCoder.encode(endDate, forKey: "end_date")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if isCampaignAccepted != nil{
            aCoder.encode(isCampaignAccepted, forKey: "is_campaign_accepted")
        }
        if postType != nil{
            aCoder.encode(postType, forKey: "post_type")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if startDate != nil{
            aCoder.encode(startDate, forKey: "start_date")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }

    }

}
