//
//  Compaign.swift
//  Influencer
//
//  Created by Joga Singh on 03/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import UIKit


class Campaign : NSObject, NSCoding{

    var campaignId : Int!
    var campaignName : String!
    var category : String!
    var categoryId : Int!
    var image : String!
    var isCampaignAccepted : String!
    var price : String!
    var status : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        campaignId = dictionary["campaign_id"] as? Int
        campaignName = dictionary["campaign_name"] as? String
        category = dictionary["category"] as? String
        categoryId = dictionary["category_id"] as? Int
        image = dictionary["image"] as? String
        isCampaignAccepted = dictionary["is_campaign_accepted"] as? String
        price = dictionary["price"] as? String
        status = dictionary["status"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if campaignId != nil{
            dictionary["campaign_id"] = campaignId
        }
        if campaignName != nil{
            dictionary["campaign_name"] = campaignName
        }
        if category != nil{
            dictionary["category"] = category
        }
        if categoryId != nil{
            dictionary["category_id"] = categoryId
        }
        if image != nil{
            dictionary["image"] = image
        }
        if isCampaignAccepted != nil{
            dictionary["is_campaign_accepted"] = isCampaignAccepted
        }
        if price != nil{
            dictionary["price"] = price
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         campaignId = aDecoder.decodeObject(forKey: "campaign_id") as? Int
         campaignName = aDecoder.decodeObject(forKey: "campaign_name") as? String
         category = aDecoder.decodeObject(forKey: "category") as? String
         categoryId = aDecoder.decodeObject(forKey: "category_id") as? Int
         image = aDecoder.decodeObject(forKey: "image") as? String
         isCampaignAccepted = aDecoder.decodeObject(forKey: "is_campaign_accepted") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if campaignId != nil{
            aCoder.encode(campaignId, forKey: "campaign_id")
        }
        if campaignName != nil{
            aCoder.encode(campaignName, forKey: "campaign_name")
        }
        if category != nil{
            aCoder.encode(category, forKey: "category")
        }
        if categoryId != nil{
            aCoder.encode(categoryId, forKey: "category_id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if isCampaignAccepted != nil{
            aCoder.encode(isCampaignAccepted, forKey: "is_campaign_accepted")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }

    }

}
