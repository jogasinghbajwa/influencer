//
//  UpdateProfileModel.swift
//  Influencer
//
//  Created by Manjodh Singh on 04/11/19.
//  Copyright © 2019 iTz_saGGu. All rights reserved.
//

import Foundation



class ProfileModel : NSObject, NSCoding{
    
    var address : String!
    var city : String!
    var country : String!
    var countryId : String!
    var dob : String!
    var email : String!
    var firstName : String!
    var gender : String!
    var isNotificationsEnabled : Bool!
    var isPhoneVerified : Bool!
    var isProfileUpdated : Bool!
    var lastName : String!
    var phone : String!
    var profileImage : String!
    var temporaryEmail : String!
    var userId : Int!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String
        city = dictionary["city"] as? String
        country = dictionary["country"] as? String
        countryId = dictionary["country_id"] as? String
        dob = dictionary["dob"] as? String
        email = dictionary["email"] as? String
        firstName = dictionary["first_name"] as? String
        gender = dictionary["gender"] as? String
        isNotificationsEnabled = dictionary["is_notifications_enabled"] as? Bool
        isPhoneVerified = dictionary["is_phone_verified"] as? Bool
        isProfileUpdated = dictionary["is_profile_updated"] as? Bool
        lastName = dictionary["last_name"] as? String
        phone = dictionary["phone"] as? String
        profileImage = dictionary["profile_image"] as? String
        temporaryEmail = dictionary["temporary_email"] as? String
        userId = dictionary["user_id"] as? Int
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if city != nil{
            dictionary["city"] = city
        }
        if country != nil{
            dictionary["country"] = country
        }
        if countryId != nil{
            dictionary["country_id"] = countryId
        }
        if dob != nil{
            dictionary["dob"] = dob
        }
        if email != nil{
            dictionary["email"] = email
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if isNotificationsEnabled != nil{
            dictionary["is_notifications_enabled"] = isNotificationsEnabled
        }
        if isPhoneVerified != nil{
            dictionary["is_phone_verified"] = isPhoneVerified
        }
        if isProfileUpdated != nil{
            dictionary["is_profile_updated"] = isProfileUpdated
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        if profileImage != nil{
            dictionary["profile_image"] = profileImage
        }
        if temporaryEmail != nil{
            dictionary["temporary_email"] = temporaryEmail
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        city = aDecoder.decodeObject(forKey: "city") as? String
        country = aDecoder.decodeObject(forKey: "country") as? String
        countryId = aDecoder.decodeObject(forKey: "country_id") as? String
        dob = aDecoder.decodeObject(forKey: "dob") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        isNotificationsEnabled = aDecoder.decodeObject(forKey: "is_notifications_enabled") as? Bool
        isPhoneVerified = aDecoder.decodeObject(forKey: "is_phone_verified") as? Bool
        isProfileUpdated = aDecoder.decodeObject(forKey: "is_profile_updated") as? Bool
        lastName = aDecoder.decodeObject(forKey: "last_name") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        profileImage = aDecoder.decodeObject(forKey: "profile_image") as? String
        temporaryEmail = aDecoder.decodeObject(forKey: "temporary_email") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? Int
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if country != nil{
            aCoder.encode(country, forKey: "country")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if dob != nil{
            aCoder.encode(dob, forKey: "dob")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if firstName != nil{
            aCoder.encode(firstName, forKey: "first_name")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if isNotificationsEnabled != nil{
            aCoder.encode(isNotificationsEnabled, forKey: "is_notifications_enabled")
        }
        if isPhoneVerified != nil{
            aCoder.encode(isPhoneVerified, forKey: "is_phone_verified")
        }
        if isProfileUpdated != nil{
            aCoder.encode(isProfileUpdated, forKey: "is_profile_updated")
        }
        if lastName != nil{
            aCoder.encode(lastName, forKey: "last_name")
        }
        if phone != nil{
            aCoder.encode(phone, forKey: "phone")
        }
        if profileImage != nil{
            aCoder.encode(profileImage, forKey: "profile_image")
        }
        if temporaryEmail != nil{
            aCoder.encode(temporaryEmail, forKey: "temporary_email")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
        
    }
    
}
