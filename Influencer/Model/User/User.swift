//
//	User.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class User : NSObject, NSCoding

{

	var isEmailVerified : Bool!
	var isNotificationsEnabled : Bool!
	var isProfileUpdated : Bool!
	var userId : Int!
	var username : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		isEmailVerified = dictionary["is_email_verified"] as? Bool
		isNotificationsEnabled = dictionary["is_notifications_enabled"] as? Bool
		isProfileUpdated = dictionary["is_profile_updated"] as? Bool
		userId = dictionary["user_id"] as? Int
		username = dictionary["username"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if isEmailVerified != nil{
			dictionary["is_email_verified"] = isEmailVerified
		}
		if isNotificationsEnabled != nil{
			dictionary["is_notifications_enabled"] = isNotificationsEnabled
		}
		if isProfileUpdated != nil{
			dictionary["is_profile_updated"] = isProfileUpdated
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		if username != nil{
			dictionary["username"] = username
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         isEmailVerified = aDecoder.decodeObject(forKey: "is_email_verified") as? Bool
         isNotificationsEnabled = aDecoder.decodeObject(forKey: "is_notifications_enabled") as? Bool
         isProfileUpdated = aDecoder.decodeObject(forKey: "is_profile_updated") as? Bool
         userId = aDecoder.decodeObject(forKey: "user_id") as? Int
         username = aDecoder.decodeObject(forKey: "username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if isEmailVerified != nil{
			aCoder.encode(isEmailVerified, forKey: "is_email_verified")
		}
		if isNotificationsEnabled != nil{
			aCoder.encode(isNotificationsEnabled, forKey: "is_notifications_enabled")
		}
		if isProfileUpdated != nil{
			aCoder.encode(isProfileUpdated, forKey: "is_profile_updated")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}
